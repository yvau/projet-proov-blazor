﻿import { tns } from 'tiny-slider/src/tiny-slider'

export function TinySlider() {
    tns({
        container: '#picture-slider',
        items: 1,
        controlsContainer: '#customize-controls',
        // navContainer: '#customize-thumbnails',
        // added
        lazyload: true,
        lazyloadSelector: '.tns-lazy',
        nav: false
        // navAsThumbnails: true,
        // autoplayTimeout: 1000,
        // fixedWidth: 600,
        // autoplayButton: '#customize-toggle',
    })
}
