import ClassicEditor from '@ckeditor/ckeditor5-build-classic'

export const CkEditor = {
    Init: function (id, dotNetReference) {
        var editors = {}
        ClassicEditor.create(document.getElementById(id)).then(function (editor) {
            editors[id] = editor
            editor.model.document.on('change:data', function () {
                var data = editor.getData()
                var el = document.createElement('div')
                el.innerHTML = data
                if (el.innerText.trim() == '') data = null
                dotNetReference.invokeMethodAsync('EditorDataChanged', data)
            })
        }).catch(function (error) {
            return console.error(error)
        })
    },
    Destroy: function (id) {
        editors[id].destroy().then(function () {
            return delete editors[id]
        }).catch(function (error) {
            return console.log(error)
        })
    }
};