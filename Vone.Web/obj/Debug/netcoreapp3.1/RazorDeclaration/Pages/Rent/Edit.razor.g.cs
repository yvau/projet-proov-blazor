#pragma checksum "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Rent\Edit.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "68714ee500ad47b1575cacd433d580806fbc0d34"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace Vone.Web.Pages.Rent
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Newtonsoft.Json.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Validation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.ModelViewResults;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/rent/{id}/edit")]
    public partial class Edit : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
