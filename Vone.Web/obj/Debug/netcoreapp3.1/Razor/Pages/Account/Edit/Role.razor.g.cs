#pragma checksum "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Account\Edit\Role.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "81efd6769674c216b878f2acb9d74b5aed88c285"
// <auto-generated/>
#pragma warning disable 1591
namespace Vone.Web.Pages.Account.Edit
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Newtonsoft.Json.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Validation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.ModelViewResults;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(DashBoardLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/account/edit/role/")]
    public partial class Role : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "container");
            __builder.AddMarkupContent(2, "\r\n   ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "flex");
            __builder.AddMarkupContent(5, "\r\n      ");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "wd-lg-250 d-none d-sm-block pr-4 bd-r pos-fixed overflow-y-auto t-0 b-0 mg-t-90");
            __builder.AddMarkupContent(8, "\r\n          ");
            __builder.OpenComponent<Vone.Web.Shared.SComponents.SEditProfile>(9);
            __builder.CloseComponent();
            __builder.AddMarkupContent(10, "\r\n      ");
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n      ");
            __builder.OpenElement(12, "div");
            __builder.AddAttribute(13, "class", "content px-4 pb-8 pd-lg-t-70 mg-md-l-250");
            __builder.AddMarkupContent(14, "\r\n          ");
            __builder.AddMarkupContent(15, "<a href=\"#\" class=\"text-sm text-gray-400\">Menu</a>\r\n         ");
            __builder.OpenElement(16, "div");
            __builder.AddAttribute(17, "class", "pt-5");
            __builder.AddMarkupContent(18, "\r\n            \r\n             ");
            __builder.OpenElement(19, "div");
            __builder.AddAttribute(20, "class", "section-block text-center text-sm-left");
            __builder.AddMarkupContent(21, "\r\n                 ");
            __builder.AddMarkupContent(22, "<h4>Profile Role</h4>\r\n                 <hr>\r\n                 \r\n                 ");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(23);
            __builder.AddAttribute(24, "EditContext", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.Forms.EditContext>(
#nullable restore
#line 20 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Account\Edit\Role.razor"
                                         _editContext

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(25, "OnSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 20 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Account\Edit\Role.razor"
                                                                  HandleFormSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(26, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(27, "\r\n                     ");
                __builder2.OpenComponent<Vone.Shared.Validation.FluentValidator<EditAccountPasswordValidator>>(28);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(29, "\r\n                     ");
                __builder2.AddMarkupContent(30, @"<div class=""visual-picker visual-picker-lg has-peek"">
                         
                         <input type=""checkbox"" id=""vpr01"" name=""vprLG""> 
                         <label class=""visual-picker-figure"" for=""vpr01"">
                             
                             <span class=""visual-picker-content""><span class=""h3 d-block"">Free</span> <span>100GB Disk Space / 2GB Bandwidth</span></span> 
                         </label> 
                         
                         <span class=""visual-picker-peek"">Started</span>
                     </div>
                     ");
                __builder2.AddMarkupContent(31, @"<div class=""visual-picker visual-picker-lg has-peek"">
                         
                         <input type=""checkbox"" id=""vpr02"" name=""vprLG""> 
                         <label class=""visual-picker-figure"" for=""vpr02"">
                             
                             <span class=""visual-picker-content""><span class=""h3 d-block"">Free</span> <span>100GB Disk Space / 2GB Bandwidth</span></span> 
                         </label> 
                         
                         <span class=""visual-picker-peek"">Started</span>
                     </div>
                     
                     ");
                __builder2.AddMarkupContent(32, @"<div class=""visual-picker visual-picker-lg has-peek"">
                         
                         <input type=""checkbox"" id=""vpr03"" name=""vprLG"" checked> 
                         <label class=""visual-picker-figure"" for=""vpr03"">
                             <span class=""visual-picker-content""><span class=""h3 d-block"">$49</span> <span>500GB Disk Space / 8GB Bandwidth</span></span> 
                         </label>
                         <span class=""visual-picker-peek"">Professional</span>
                     </div>
                     ");
                __builder2.AddMarkupContent(33, @"<div class=""visual-picker visual-picker-lg has-peek"">
                         <input type=""checkbox"" id=""vpr04"" name=""vprLG"">
                         <label class=""visual-picker-figure"" for=""vpr04"">
                             
                             <span class=""visual-picker-content""><span class=""h3 d-block"">$99</span> <span>Unlimited Disk Space / Unlimited Bandwidth</span></span> 
                         </label> 
                         
                         <span class=""visual-picker-peek"">Bussiness</span>
                     </div>
                     <hr>

                     ");
                __builder2.AddMarkupContent(34, "<div class=\"form-group\">\r\n                         <button type=\"submit\" class=\"btn btn-primary\">Update Password</button>\r\n                     </div>\r\n                 ");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(35, "\r\n             ");
            __builder.CloseElement();
            __builder.AddMarkupContent(36, "\r\n            \r\n            <hr class=\"my-5\">\r\n            \r\n         ");
            __builder.CloseElement();
            __builder.AddMarkupContent(37, "\r\n         ");
            __builder.OpenComponent<Vone.Web.Shared.DashboardFooter>(38);
            __builder.CloseComponent();
            __builder.AddMarkupContent(39, "\r\n      ");
            __builder.CloseElement();
            __builder.AddMarkupContent(40, "\r\n   ");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 76 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Account\Edit\Role.razor"
       

    private EditContext _editContext; // edit context validation purposes
    private ProfileModelView _profileModel = new ProfileModelView(); // Profile model

    protected override void OnInitialized()
    {
        _editContext = new EditContext(_profileModel); // we bind edit context to profile model
    }

    private void HandleFormSubmit()
    {
        var isValid = _editContext.Validate();
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
