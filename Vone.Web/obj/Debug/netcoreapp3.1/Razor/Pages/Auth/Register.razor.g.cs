#pragma checksum "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e5078380c8785956a0e95105293e7b3aa013dac0"
// <auto-generated/>
#pragma warning disable 1591
namespace Vone.Web.Pages.Auth
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Newtonsoft.Json.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Validation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.ModelViewResults;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/auth/register")]
    public partial class Register : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "section");
            __builder.AddAttribute(1, "class", "content content-auth mg-md-t-70");
            __builder.AddMarkupContent(2, "\r\n    ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "container");
            __builder.AddMarkupContent(5, "\r\n        ");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "media align-items-stretch justify-content-center ht-100p pos-relative");
            __builder.AddMarkupContent(8, "\r\n            ");
            __builder.AddMarkupContent(9, "<div class=\"media-body align-items-center d-none d-lg-flex\">\r\n                <div class=\"mx-wd-600\">\r\n                    <img src=\"/images/image_registration.png\" class=\"img-fluid\" alt>\r\n                </div>\r\n            </div>\r\n            ");
            __builder.OpenElement(10, "div");
            __builder.AddAttribute(11, "class", "sign-wrapper mg-lg-l-50 mg-xl-l-60");
            __builder.AddMarkupContent(12, "\r\n                ");
            __builder.OpenElement(13, "div");
            __builder.AddAttribute(14, "class", "pd-t-20 wd-100p");
            __builder.AddMarkupContent(15, "\r\n                    ");
            __builder.AddMarkupContent(16, "<h4 class=\"tx-color-01 mg-b-5\">Create New Account</h4>\r\n                    ");
            __builder.AddMarkupContent(17, "<p class=\"tx-color-03 tx-16 mg-b-40\">It\'s free to signup and only takes a minute.</p>\r\n                    ");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(18);
            __builder.AddAttribute(19, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 18 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                      _profileModelView

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(20, "OnSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 18 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                    HandleFormSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(21, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(22, "\r\n                        ");
                __builder2.OpenComponent<Vone.Shared.Validation.FluentValidator<RegisterValidator>>(23);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(24, "\r\n");
#nullable restore
#line 20 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                         if (!string.IsNullOrEmpty(_apiResponse.Message))
                        {

#line default
#line hidden
#nullable disable
                __builder2.AddContent(25, "                            ");
                __builder2.OpenComponent<Vone.Web.Shared.Component.AlertMessage>(26);
                __builder2.AddAttribute(27, "Success", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 22 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                              _apiResponse.IsSuccess

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(28, "Message", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 22 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                                                                _apiResponse.Message

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(29, "\r\n");
#nullable restore
#line 23 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                        }

#line default
#line hidden
#nullable disable
                __builder2.AddContent(30, "                        ");
                __builder2.OpenElement(31, "div");
                __builder2.AddAttribute(32, "class", "form-group");
                __builder2.AddMarkupContent(33, "\r\n                            ");
                __builder2.AddMarkupContent(34, "<label>Firstname</label>\r\n                            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(35);
                __builder2.AddAttribute(36, "class", "form-control");
                __builder2.AddAttribute(37, "placeholder", "Enter your firstname");
                __builder2.AddAttribute(38, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 26 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                                                            _profileModelView.FirstName

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(39, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _profileModelView.FirstName = __value, _profileModelView.FirstName))));
                __builder2.AddAttribute(40, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => _profileModelView.FirstName));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(41, "\r\n                            ");
                __Blazor.Vone.Web.Pages.Auth.Register.TypeInference.CreateValidationMessage_0(__builder2, 42, 43, 
#nullable restore
#line 27 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                      () => _profileModelView.FirstName

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(44, "\r\n                        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(45, "\r\n                        ");
                __builder2.OpenElement(46, "div");
                __builder2.AddAttribute(47, "class", "form-group");
                __builder2.AddMarkupContent(48, "\r\n                            ");
                __builder2.AddMarkupContent(49, "<label>Lastname</label>\r\n                            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(50);
                __builder2.AddAttribute(51, "class", "form-control");
                __builder2.AddAttribute(52, "placeholder", "Enter your lastname");
                __builder2.AddAttribute(53, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 31 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                                                           _profileModelView.LastName

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(54, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _profileModelView.LastName = __value, _profileModelView.LastName))));
                __builder2.AddAttribute(55, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => _profileModelView.LastName));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(56, "\r\n                            ");
                __Blazor.Vone.Web.Pages.Auth.Register.TypeInference.CreateValidationMessage_1(__builder2, 57, 58, 
#nullable restore
#line 32 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                      () => _profileModelView.LastName

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(59, "\r\n                        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(60, "\r\n                        ");
                __builder2.OpenElement(61, "div");
                __builder2.AddAttribute(62, "class", "form-group");
                __builder2.AddMarkupContent(63, "\r\n                            ");
                __builder2.AddMarkupContent(64, "<label>Email address</label>\r\n                            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(65);
                __builder2.AddAttribute(66, "class", "form-control");
                __builder2.AddAttribute(67, "placeholder", "Enter your email address");
                __builder2.AddAttribute(68, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 36 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                                                                _profileModelView.Email

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(69, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _profileModelView.Email = __value, _profileModelView.Email))));
                __builder2.AddAttribute(70, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => _profileModelView.Email));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(71, "\r\n                            ");
                __Blazor.Vone.Web.Pages.Auth.Register.TypeInference.CreateValidationMessage_2(__builder2, 72, 73, 
#nullable restore
#line 37 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                      () => _profileModelView.Email

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(74, "\r\n                        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(75, "\r\n                        ");
                __builder2.OpenElement(76, "div");
                __builder2.AddAttribute(77, "class", "form-group");
                __builder2.AddMarkupContent(78, "\r\n                            ");
                __builder2.AddMarkupContent(79, "<label>Password</label>\r\n                            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(80);
                __builder2.AddAttribute(81, "type", "password");
                __builder2.AddAttribute(82, "class", "form-control");
                __builder2.AddAttribute(83, "placeholder", "Enter your password");
                __builder2.AddAttribute(84, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 41 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                                                                                           _profileModelView.Password

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(85, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => _profileModelView.Password = __value, _profileModelView.Password))));
                __builder2.AddAttribute(86, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => _profileModelView.Password));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(87, "\r\n                            ");
                __Blazor.Vone.Web.Pages.Auth.Register.TypeInference.CreateValidationMessage_3(__builder2, 88, 89, 
#nullable restore
#line 42 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                                      () => _profileModelView.Password

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(90, "\r\n                        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(91, "\r\n                        ");
                __builder2.AddMarkupContent(92, "<div class=\"form-group tx-12\">\r\n                            By clicking <strong>Create an account</strong> below, you agree to our terms of service and privacy statement.\r\n                        </div>\r\n\r\n                        ");
                __builder2.OpenElement(93, "button");
                __builder2.AddAttribute(94, "class", (
#nullable restore
#line 48 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
                                        _loadingButton

#line default
#line hidden
#nullable disable
                ) + " btn" + " btn-primary" + " btn-block");
                __builder2.AddContent(95, "Create Account");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(96, "\r\n                    ");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(97, "\r\n                    ");
            __builder.AddMarkupContent(98, "<div class=\"divider-text\">or</div>\r\n                    ");
            __builder.AddMarkupContent(99, "<button class=\"btn btn-outline-primary btn-block\">Sign Up With Facebook</button>\r\n                    ");
            __builder.AddMarkupContent(100, "<button class=\"btn btn-outline-danger btn-block\">Sign Up With Google</button>\r\n                    ");
            __builder.AddMarkupContent(101, "<div class=\"tx-13 mg-t-20 tx-center\">Already have an account? <a href=\"/auth/login/\">Sign In</a></div>\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(102, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(103, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(104, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(105, "\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 60 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Auth\Register.razor"
       
    private ProfileModelView _profileModelView { get; set; } = new ProfileModelView(); // Profile model
    private ApiResponse _apiResponse = new ApiResponse(); // to  display alert when success
    private bool _processing;
    string _loadingButton => _processing ? "loading" : null;
    private bool showPassword;

    [Parameter]
    public EventCallback<string> PasswordChanged { get; set; }


    private async Task HandleFormSubmit(EditContext _editContext)
    {
        _processing = true;
        var isValid = _editContext.Validate();

        if (isValid)
        {
            // we bind values manually
            _profileModelView.IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();

            // we post the data
            var _response = await _http.PostAsJsonAsync($"api/Profiles", _profileModelView);

            // we show success message
            _apiResponse.Message = $"A Message has been sent to your email {_profileModelView.Email}";
            _apiResponse.IsSuccess = true;

            _profileModelView = new ProfileModelView(); // clear the form
        }

        _processing = false;
    }

    private Task OnPasswordChanged(ChangeEventArgs e)
    {
        // EventCallback.Factory.CreateBinder<string>(this, __value => CurrentValueAsString = __value, CurrentValueAsString)

        _profileModelView.Password = e.Value.ToString();

        return PasswordChanged.InvokeAsync(_profileModelView.Password);
    }

    private void ToggleShowPassword()
    {
        showPassword = !showPassword;
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor _accessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient _http { get; set; }
    }
}
namespace __Blazor.Vone.Web.Pages.Auth.Register
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateValidationMessage_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
