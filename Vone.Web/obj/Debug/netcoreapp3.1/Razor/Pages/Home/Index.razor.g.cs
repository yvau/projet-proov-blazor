#pragma checksum "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Home\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "04f4c392df54486c30ced6c9ac9884bf16d85b9c"
// <auto-generated/>
#pragma warning disable 1591
namespace Vone.Web.Pages.Home
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Newtonsoft.Json.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Validation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.ModelViewResults;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<section class=""mg-md-t-70"">
    
    <div class=""container"">
        
        <div class=""row align-items-center"">
            
            <div class=""col-12 col-md-6 order-md-2"">
                <img class=""img-fluid mb-5 mb-md-0"" src=""/images/svg/launch.svg"" alt>
            </div>
            
            <div class=""col-12 col-md-6 order-md-1"">
                <div class=""col-fix ml-auto text-center text-sm-left"">
                    <h1 class=""mb-4""> At<strong> Vone +</strong> we build <strong>an amazing experience</strong>! </h1>
                    <p class=""lead text-muted mb-5""> Between seller or lessor and buyer or tenant. Helping you to find match accurately your needs in terms of real estates. </p><a href=""/auth/register"" class=""btn btn-lg btn-primary d-block d-sm-inline-block mr-sm-2 my-3"">Let's Try <i class=""fa fa-angle-right ml-2""></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
");
            __builder.AddMarkupContent(1, "<section class=\"py-5\">\r\n    <div class=\"container\">\r\n        \r\n        <div class=\"wd-md-80p wd-lg-60p text-center mx-md-auto mg-b-80\">\r\n            <h2>Trusted by Ngo, enterprise, and more than 33,000 of you</h2>\r\n        </div>\r\n        \r\n        \r\n        <div class=\"row justify-content-sm-center tx-center\">\r\n            <div class=\"col-4 col-sm-2 mg-b-50\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/slack.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/stripe.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/airbnb.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/weebly.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/spotify.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/uber.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"d-none d-sm-block wd-100p\"></div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50 mb-sm-0\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/shopify.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50 mb-sm-0\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/fitbit.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2 mg-b-50 mb-sm-0\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/hubspot.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/kaplan.svg\" alt=\"Image Description\">\r\n            </div>\r\n            <div class=\"col-4 col-sm-2\">\r\n                <img class=\"mx-wd-90 mx-wd-md-110 mg-x-auto\" src=\"/images/svg/paypal.svg\" alt=\"Image Description\">\r\n            </div>\r\n        </div>\r\n        \r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(2, "<section class=\"py-5\">\r\n    <div class=\"container\">\r\n        <div class=\"w-md-80 w-lg-60 text-center mx-md-auto mb-5\">\r\n            <h2>Our featured properties by area</h2>\r\n            <p>We\'ve helped some great companies brand, design and get to market.</p>\r\n        </div>\r\n        <div class=\"row\">\r\n\r\n            <div class=\"order-lg-2 col-sm-6 col-lg-4 mb-3 mb-sm-7\">\r\n                \r\n                <article class=\"card align-items-start flex-wrap flex-row ht-lg-180 gradient-y-overlay-sm-dark bg-img-hero rounded transition-3d-hover\" style=\"background-image: url(/images/img/tnt.jpg);\">\r\n                    <div class=\"card-header bd-0 bg-transparent wd-100p\">\r\n                        <a class=\"small tx-white tx-bold tx-uppercase mr-2\" href=\"/property/p/list/\">Toronto</a>\r\n                    </div>\r\n\r\n                    <div class=\"card-footer bd-0 bg-transparent mt-auto\">\r\n                        <a href=\"/property/p/list/\">\r\n                            <h3 class=\"tx-white\">80,789</h3>\r\n                            <span class=\"tx-white-70\">See more <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                        </a>\r\n                    </div>\r\n                </article>\r\n                \r\n                \r\n                <article class=\"card align-items-start flex-wrap flex-row ht-lg-180 gradient-y-overlay-sm-dark bg-img-hero rounded transition-3d-hover\" style=\"background-image: url(/images/img/qbc.jpg);\">\r\n                    <div class=\"card-header bd-0 bg-transparent wd-100p\">\r\n                        <a class=\"small tx-white tx-bold tx-uppercase mr-2\" href=\"/property/p/list/\">Quebec city</a>\r\n                    </div>\r\n\r\n                    <div class=\"card-footer bd-0 bg-transparent mt-auto\">\r\n                        <a href=\"/property/p/list/\">\r\n                            <h3 class=\"tx-white\">34,897</h3>\r\n                            <span class=\"tx-white-70\">See more <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                        </a>\r\n                    </div>\r\n                </article>\r\n                \r\n            </div>\r\n\r\n            <div class=\"order-lg-5 col-sm-6 col-lg-4 mb-3 mb-sm-7\">\r\n                \r\n                <article class=\"card align-items-start flex-wrap flex-row ht-lg-380 gradient-y-overlay-sm-dark bg-img-hero rounded transition-3d-hover\" style=\"background-image: url(/images/img/img1.jpg);\">\r\n                    <div class=\"card-header bd-0 bg-transparent wd-100p\">\r\n                        <a class=\"small tx-white tx-bold tx-uppercase mr-2\" href=\"#\">Montréal</a>\r\n                    </div>\r\n\r\n                    <div class=\"card-footer bd-0 bg-transparent mt-auto\">\r\n                        <a href=\"#\">\r\n                            <h3 class=\"tx-white\">146,534 properties</h3>\r\n                            <span class=\"tx-white-70\">See more <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                        </a>\r\n                    </div>\r\n                </article>\r\n                \r\n            </div>\r\n\r\n            <div class=\"order-lg-7 col-sm-6 col-lg-4 mb-3 mb-sm-7\">\r\n                \r\n                <article class=\"card align-items-start flex-wrap flex-row ht-lg-180 gradient-y-overlay-sm-dark bg-img-hero rounded transition-3d-hover\" style=\"background-image: url(/images/img/img5.jpg);\">\r\n                    <div class=\"card-header bd-0 bg-transparent wd-100p\">\r\n                        <a class=\"small tx-white tx-bold tx-uppercase mr-2\" href=\"#\">Vancouver</a>\r\n                    </div>\r\n\r\n                    <div class=\"card-footer bd-0 bg-transparent mt-auto\">\r\n                        <a href=\"#\">\r\n                            <h3 class=\"tx-white\">101,365 properties</h3>\r\n                            <span class=\"tx-white-70\">See more <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                        </a>\r\n                    </div>\r\n                </article>\r\n                \r\n                \r\n                <article class=\"card align-items-start flex-wrap flex-row ht-lg-180 gradient-y-overlay-sm-dark bg-img-hero rounded transition-3d-hover\" style=\"background-image: url(/images/img/img56.jpg);\">\r\n                    <div class=\"card-header bd-0 bg-transparent wd-100p\">\r\n                        <a class=\"small tx-white tx-bold tx-uppercase mr-2\" href=\"#\">Winnipeg</a>\r\n                    </div>\r\n\r\n                    <div class=\"card-footer bd-0 bg-transparent mt-auto\">\r\n                        <a href=\"#\">\r\n                            <h3 class=\"tx-white\">47,987 properties</h3>\r\n                            <span class=\"tx-white-70\">See more <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                        </a>\r\n                    </div>\r\n                </article>\r\n                \r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(3, "<section>\r\n    <div class=\"container\">\r\n        <div class=\"wd-md-80p wd-lg-60p text-center mg-md-x-auto mb-5\">\r\n            <h2>Most proposals by cities</h2>\r\n            <p>We\'ve helped some great companies brand, design and get to market.</p>\r\n        </div>\r\n        <div class=\"row\">\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Toronto\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">34,082,020</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Vancouver\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">24,087,786</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Montreal\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">27,798,543</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Ottawa\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">10,889,020</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Calgary\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">23,408,090</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Quebec city\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">12,765,789</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Victorias\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">396,540</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Edmonto\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">49,020</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            <article class=\"col-md-4 mb-3\">\r\n                <a class=\"card card-frame p-3\" href=\"#\">\r\n                    <div class=\"media align-items-center\">\r\n                        <div class=\"media-body mr-2\">\r\n                            <h4 class=\"h6 mb-0\">Winnipeg\'s proposals</h4>\r\n                            <span class=\"d-block font-size-1 text-body\">78,530</span>\r\n                        </div>\r\n                        <i class=\"fas fa-angle-right\"></i>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(4, "<section class=\"py-2\">\r\n    <div class=\"container\">\r\n        \r\n        <div class=\"w-md-80 w-lg-50 text-center mx-md-auto mb-5 mb-md-9\">\r\n            <h2 class=\"h1\">Read our latest news</h2>\r\n            <p>We\'ve helped some great companies brand, design and get to market.</p>\r\n        </div>\r\n        \r\n\r\n        <div class=\"row mx-n2 mb-5 mb-md-9\">\r\n            <div class=\"col-sm-6 col-lg-3 px-2 mb-3 mb-lg-0\">\r\n                \r\n                <a class=\"card h-100 transition-3d-hover\" href=\"#\">\r\n                    <img class=\"card-img-top\" src=\"/images/img/img9.jpg\" alt=\"Image Description\">\r\n                    <div class=\"card-body\">\r\n                        <span class=\"d-block small tx-bold tx-uppercase mb-2\">Product</span>\r\n                        <h5 class=\"mb-0\">Better is when everything works together</h5>\r\n                    </div>\r\n                </a>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-6 col-lg-3 px-2 mb-3 mb-lg-0\">\r\n                \r\n                <a class=\"card h-100 transition-3d-hover\" href=\"#\">\r\n                    <img class=\"card-img-top\" src=\"/images/img/img10.jpg\" alt=\"Image Description\">\r\n                    <div class=\"card-body\">\r\n                        <span class=\"d-block small tx-bold tx-uppercase mb-2\">Business</span>\r\n                        <h5 class=\"mb-0\">What CFR really is about</h5>\r\n                    </div>\r\n                </a>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-6 col-lg-3 px-2 mb-3 mb-sm-0\">\r\n                \r\n                <a class=\"card h-100 transition-3d-hover\" href=\"#\">\r\n                    <img class=\"card-img-top\" src=\"/images/img/img11.jpg\" alt=\"Image Description\">\r\n                    <div class=\"card-body\">\r\n                        <span class=\"d-block small tx-bold tx-uppercase mb-2\">Business</span>\r\n                        <h5 class=\"mb-0\">Should Product Owners think like entrepreneurs?</h5>\r\n                    </div>\r\n                </a>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-6 col-lg-3 px-2\">\r\n                \r\n                <a class=\"card h-100 transition-3d-hover\" href=\"#\">\r\n                    <img class=\"card-img-top\" src=\"/images/img/img12.jpg\" alt=\"Image Description\">\r\n                    <div class=\"card-body\">\r\n                        <span class=\"d-block small tx-bold tx-uppercase mb-2\">Facilitate</span>\r\n                        <h5 class=\"mb-0\">Announcing Front Strategies: Ready-to-use rules</h5>\r\n                    </div>\r\n                </a>\r\n                \r\n            </div>\r\n        </div>\r\n\r\n        \r\n        <div class=\"position-relative z-index-2 text-center\">\r\n            <div class=\"d-inline-block font-size-1 border bg-white text-center rounded-pill py-3 px-4\">\r\n                Check the latest blogs stories? <a class=\"tx-bold ml-3\" href=\"/blogs/\">Go here <span class=\"fas fa-angle-right ml-1\"></span></a>\r\n            </div>\r\n        </div>\r\n        \r\n    </div>\r\n\r\n</section>\r\n");
            __builder.AddMarkupContent(5, "<section>\r\n    <div class=\"container py-5\">\r\n        \r\n        <div class=\"w-md-80 w-lg-50 text-center mx-md-auto mb-5 mb-md-9\">\r\n            <span class=\"d-block small tx-bold tx-uppercase mb-2\">What we do?</span>\r\n            <h2>Front makes designing easy and performance fast</h2>\r\n        </div>\r\n        \r\n\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-4 mb-3 mb-sm-5\">\r\n                \r\n                <div class=\"pr-lg-6\">\r\n                    <figure class=\"mx-wd-45 wd-100p mb-3\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-45.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h4>More creativity</h4>\r\n                    <p>This is where we really begin to visualize your napkin sketches and make them into beautiful pixels.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-4 mb-3 mb-sm-5\">\r\n                \r\n                <div class=\"pr-lg-6\">\r\n                    <figure class=\"mx-wd-45 wd-100p mb-3\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-5.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h4>Scale budgets efficiently</h4>\r\n                    <p>Now that we\'ve aligned the details, it\'s time to get things mapped out and organized.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-4 mb-3 mb-sm-5\">\r\n                \r\n                <div class=\"pr-lg-6\">\r\n                    <figure class=\"mx-wd-45 wd-100p mb-3\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-2.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h4>Smart Dashboards</h4>\r\n                    <p>This is where we really begin to visualize your napkin sketches and make them into beautiful pixels.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-4 mb-3 mb-sm-5 mb-sm-0\">\r\n                \r\n                <div class=\"pr-lg-6\">\r\n                    <figure class=\"mx-wd-45 wd-100p mb-3\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-1.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h4>Control Center</h4>\r\n                    <p>Now that we\'ve aligned the details, it\'s time to get things mapped out and organized.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-4 mb-3 mb-sm-0\">\r\n                \r\n                <div class=\"pr-lg-6\">\r\n                    <figure class=\"mx-wd-45 wd-100p mb-3\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-15.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h4>Email Reports</h4>\r\n                    <p>We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-sm-4\">\r\n                \r\n                <div class=\"pr-lg-6\">\r\n                    <figure class=\"mx-wd-45 wd-100p mb-3\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-26.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h4>Forecasting</h4>\r\n                    <p>Staying focused allows us to turn every project we complete into something we love.</p>\r\n                </div>\r\n                \r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(6, "<section>\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-12\">\r\n\r\n                \r\n                <div class=\"card-group card-border card-border-lg border-primary shadow-light-lg\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-body\">\r\n\r\n                            \r\n                            <h2 class=\"tx-bold text-center mb-0\">\r\n                                <span data-toggle=\"countup\" class=\"counted\">15</span>M+\r\n                            </h2>\r\n\r\n                            \r\n                            <p class=\"text-center text-muted mb-0\">\r\n                                Active users\r\n                            </p>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card border-left-md\">\r\n                        <div class=\"card-body\">\r\n\r\n                            \r\n                            <h2 class=\"tx-bold text-center mb-0\">\r\n                                0\r\n                            </h2>\r\n\r\n                            \r\n                            <p class=\"text-center text-muted mb-0\">\r\n                                Data breaches\r\n                            </p>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card border-left-md\">\r\n                        <div class=\"card-body\">\r\n\r\n                            \r\n                            <h2 class=\"tx-bold text-center mb-0\">\r\n                                <span class=\"counted\">99.99</span>%\r\n                            </h2>\r\n\r\n                            \r\n                            <p class=\"text-center text-muted mb-0\">\r\n                                Global uptime\r\n                            </p>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card border-left-md\">\r\n                        <div class=\"card-body\">\r\n\r\n                            \r\n                            <h2 class=\"tx-bold text-center mb-0\">\r\n                                <span class=\"counted\">95</span>M+\r\n                            </h2>\r\n\r\n                            \r\n                            <p class=\"text-center text-muted mb-0\">\r\n                                Terabytes\r\n                            </p>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div> \r\n    </div> \r\n</section>\r\n");
            __builder.OpenComponent<Vone.Web.Shared.MainFooter>(7);
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 495 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Home\Index.razor"
       
    private string myMarkup =
        "<p class='markup'>This is a <em>markup string</em>.</p>";

    /*private void ShowModal()
    {
        _modalService.Show("Simple Form", typeof(SimpleForm));
    }*/

    /*[CascadingParameter]
protected Task<AuthenticationState> AuthState { get; set; }

protected override async Task OnAfterRenderAsync(bool firstRender)
{
    ClaimsPrincipal user = (await AuthState).User;

    if (user.Identity.IsAuthenticated)
    {
        Console.WriteLine($"{user.Identity.Name} is authenticated.");
    }
}*/

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
