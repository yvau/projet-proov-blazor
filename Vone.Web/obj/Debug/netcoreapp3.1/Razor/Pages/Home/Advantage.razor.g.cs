#pragma checksum "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Home\Advantage.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d97348f47f2637a8f403f977c26cde52ec327329"
// <auto-generated/>
#pragma warning disable 1591
namespace Vone.Web.Pages.Home
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Newtonsoft.Json.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Validation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.ModelViewResults;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/advantage")]
    public partial class Advantage : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<section class=""mg-md-t-70"">
    
    <div class=""container"">
        
        <div class=""row align-items-center"">
            
            <div class=""col-12 col-md-6 order-md-2"">
                <img class=""img-fluid mb-4 mb-md-0"" src=""https://uselooper.com/assets/images/illustration/testimony.svg"" alt>
            </div>
            
            <div class=""col-12 col-md-6 order-md-1 text-center text-sm-left"">
                <h1 class=""mb-4"">You will improve your <strong>response, reactivity and time</strong> </h1>
                <p class=""lead text-muted mb-4""> will get accurate request in demand and response from seller /lessor. The same way for properties matching you needs </p>
            </div>
        </div>
    </div>
</section>
");
            __builder.AddMarkupContent(1, "<section class=\"pt-5\">\r\n    \r\n    <div class=\"container position-relative\">\r\n        <h2 class=\"text-center text-sm-left\"> Get more benefits </h2>\r\n        <p class=\"lead text-muted text-center text-sm-left mb-5\"> Scaffold your theme and start developing quickly. </p>\r\n        <div class=\"card-deck-lg\">\r\n            \r\n            <div class=\"card shadow aos-init aos-animate\" data-aos=\"fade-up\" data-aos-delay=\"0\">\r\n                \r\n                <div class=\"card-body p-4\">\r\n                    <div class=\"d-sm-flex align-items-start text-center text-sm-left\">\r\n                        <img src=\"https://uselooper.com/assets/images/illustration/rocket.svg\" class=\"mr-sm-4 mb-3 mb-sm-0\" alt width=\"72\">\r\n                        <div class=\"flex-fill\">\r\n                            <h5 class=\"mt-0\"> Ready-to-go </h5>\r\n                            <p class=\"text-muted font-size-lg\"> Provides a tons of useful Components and Pages that allow you to focus on your application logic rather than re-touching pixels. </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            \r\n            <div class=\"card shadow aos-init aos-animate\" data-aos=\"fade-up\" data-aos-delay=\"100\">\r\n                \r\n                <div class=\"card-body p-4\">\r\n                    <div class=\"d-sm-flex align-items-start text-center text-sm-left\">\r\n                        <img src=\"https://uselooper.com/assets/images/illustration/setting.svg\" class=\"mr-sm-4 mb-3 mb-sm-0\" alt width=\"72\">\r\n                        <div class=\"flex-fill\">\r\n                            <h5 class=\"mt-0\"> Highly functionality </h5>\r\n                            <p class=\"text-muted font-size-lg\"> Looper is not 1001 in 1 shit theme, there\'s not an obscene amount of wasted space, and we are carefully in adding useful features without going overboard. </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        \r\n        <div class=\"card-deck-lg\">\r\n            \r\n            <div class=\"card shadow aos-init aos-animate\" data-aos=\"fade-up\" data-aos-delay=\"200\">\r\n                \r\n                <div class=\"card-body p-4\">\r\n                    <div class=\"d-sm-flex align-items-start text-center text-sm-left\">\r\n                        <img src=\"https://uselooper.com/assets/images/illustration/brain.svg\" class=\"mr-sm-4 mb-3 mb-sm-0\" alt width=\"72\">\r\n                        <div class=\"flex-fill\">\r\n                            <h5 class=\"mt-0\"> Boost your creativity </h5>\r\n                            <p class=\"text-muted font-size-lg\"> The design is clean, simple, modern, and essential, with Lots of refined graphics and tons of 3rd party support. </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            \r\n            <div class=\"card shadow aos-init aos-animate\" data-aos=\"fade-up\" data-aos-delay=\"300\">\r\n                \r\n                <div class=\"card-body p-4\">\r\n                    <div class=\"d-sm-flex align-items-start text-center text-sm-left\">\r\n                        <img src=\"https://uselooper.com/assets/images/illustration/horse.svg\" class=\"mr-sm-4 mb-3 mb-sm-0\" alt width=\"72\">\r\n                        <div class=\"flex-fill\">\r\n                            <h5 class=\"mt-0\"> Simple workflow </h5>\r\n                            <p class=\"text-muted font-size-lg\"> We make sure you can make a magic by running just two command. It was very easy, extremely powerful, and modern. </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        \r\n        <div class=\"card-deck-lg\">\r\n            \r\n            <div class=\"card shadow aos-init aos-animate\" data-aos=\"fade-up\" data-aos-delay=\"400\">\r\n                \r\n                <div class=\"card-body p-4\">\r\n                    <div class=\"d-sm-flex align-items-start text-center text-sm-left\">\r\n                        <img src=\"https://uselooper.com/assets/images/illustration/savings.svg\" class=\"mr-sm-4 mb-3 mb-sm-0\" alt width=\"72\">\r\n                        <div class=\"flex-fill\">\r\n                            <h5 class=\"mt-0\"> One time payment </h5>\r\n                            <p class=\"text-muted font-size-lg\"> After you make a purchase you will get a free update forever. We will make Looper better and better again for you. </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            \r\n            <div class=\"card shadow aos-init aos-animate\" data-aos=\"fade-up\" data-aos-delay=\"500\">\r\n                \r\n                <div class=\"card-body p-4\">\r\n                    <div class=\"d-sm-flex align-items-start text-center text-sm-left\">\r\n                        <img src=\"https://uselooper.com/assets/images/illustration/document.svg\" class=\"mr-sm-4 mb-3 mb-sm-0\" alt width=\"72\">\r\n                        <div class=\"flex-fill\">\r\n                            <h5 class=\"mt-0\"> Well Documented </h5>\r\n                            <p class=\"text-muted font-size-lg\"> Each component is well explained with example, code block and best practices. We also provide extensive information about customizing Looper. </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(2, "<section class=\"pt-5 py-md-11\">\r\n    <div class=\"container\">\r\n        <div class=\"row align-items-center justify-content-between\">\r\n            <div class=\"col-12 col-md-5 order-md-2\">\r\n\r\n                \r\n                <span class=\"badge badge-success-soft badge-pill mb-3\">\r\n                    <span class=\"h6 text-uppercase\">\r\n                        Benefits\r\n                    </span>\r\n                </span>\r\n\r\n                \r\n                <h2>\r\n                    We\'ll take of you with <br>\r\n                    <span class=\"text-success\">a great benefits package</span>.\r\n                </h2>\r\n\r\n                \r\n                <p class=\"font-size-lg text-muted mb-6 mb-md-0\">\r\n                    We put our money where our mouthes are. Once you\'re part of our team, we\'re going to take the best possible care of you with tons of benefits and perks unavailable anywhere else.\r\n                </p>\r\n\r\n            </div>\r\n            <div class=\"col-12 col-md-6 order-md-1\">\r\n\r\n                \r\n                <div class=\"card card-border border-success shadow-lg\">\r\n                    <div class=\"card-body\">\r\n\r\n                        \r\n                        <div class=\"list-group list-group-flush\">\r\n                            <div class=\"list-group-item d-flex align-items-center\">\r\n\r\n                                \r\n                                <div class=\"mr-auto\">\r\n\r\n                                    \r\n                                    <p class=\"font-weight-bold mb-1\">\r\n                                        Comprehensive benefits\r\n                                    </p>\r\n\r\n                                    \r\n                                    <p class=\"font-size-sm text-muted mb-0\">\r\n                                        Health, dental, vision, 401k, and more.\r\n                                    </p>\r\n\r\n                                </div>\r\n\r\n                                \r\n                                <div class=\"badge badge-rounded-circle badge-success-soft ml-4\">\r\n                                    <i class=\"fe fe-check\"></i>\r\n                                </div>\r\n\r\n                            </div>\r\n                            <div class=\"list-group-item d-flex align-items-center\">\r\n\r\n                                \r\n                                <div class=\"mr-auto\">\r\n\r\n                                    \r\n                                    <p class=\"font-weight-bold mb-1\">\r\n                                        Unlimited time off\r\n                                    </p>\r\n\r\n                                    \r\n                                    <p class=\"font-size-sm text-muted mb-0\">\r\n                                        Vacation on your own terms.\r\n                                    </p>\r\n\r\n                                </div>\r\n\r\n                                \r\n                                <div class=\"badge badge-rounded-circle badge-success-soft ml-4\">\r\n                                    <i class=\"fe fe-check\"></i>\r\n                                </div>\r\n\r\n                            </div>\r\n                            <div class=\"list-group-item d-flex align-items-center\">\r\n\r\n                                \r\n                                <div class=\"mr-auto\">\r\n\r\n                                    \r\n                                    <p class=\"font-weight-bold mb-1\">\r\n                                        Cutting edge hardware\r\n                                    </p>\r\n\r\n                                    \r\n                                    <p class=\"font-size-sm text-muted mb-0\">\r\n                                        We provide brand new computers and phones.\r\n                                    </p>\r\n\r\n                                </div>\r\n\r\n                                \r\n                                <div class=\"badge badge-rounded-circle badge-success-soft ml-4\">\r\n                                    <i class=\"fe fe-check\"></i>\r\n                                </div>\r\n\r\n                            </div>\r\n                            <div class=\"list-group-item d-flex align-items-center\">\r\n\r\n                                \r\n                                <div class=\"mr-auto\">\r\n\r\n                                    \r\n                                    <p class=\"font-weight-bold mb-1\">\r\n                                        Moving assistance\r\n                                    </p>\r\n\r\n                                    \r\n                                    <p class=\"font-size-sm text-muted mb-0\">\r\n                                        We will help you get here to work with us!\r\n                                    </p>\r\n\r\n                                </div>\r\n\r\n                                \r\n                                <div class=\"badge badge-rounded-circle badge-success-soft ml-4\">\r\n                                    <i class=\"fe fe-check\"></i>\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div> \r\n    </div> \r\n</section>\r\n");
            __builder.AddMarkupContent(3, "<section>\r\n    <div class=\"container space-top-2 space-top-lg-3 space-bottom-lg-2\">\r\n        \r\n        <div class=\"w-md-80 w-lg-50 text-center mx-md-auto mb-5 mb-md-9\">\r\n            <span class=\"d-block small font-weight-bold text-cap mb-2\">Always improving</span>\r\n            <h2>Solutions rooted in code and design</h2>\r\n        </div>\r\n        \r\n        \r\n        <ul class=\"step step-md step-centered\">\r\n            <li class=\"step-item\">\r\n                <div class=\"step-content-wrapper\">\r\n                    <span class=\"step-icon step-icon-soft-primary\">1</span>\r\n                    <div class=\"step-content\">\r\n                        <h3>Industry-leading designs</h3>\r\n                        <p>Achieve virtually any design and layout from within the one template.</p>\r\n                    </div>\r\n                </div>\r\n            </li>\r\n\r\n            <li class=\"step-item\">\r\n                <div class=\"step-content-wrapper\">\r\n                    <span class=\"step-icon step-icon-soft-primary\">2</span>\r\n                    <div class=\"step-content\">\r\n                        <h3>Learn from the docs</h3>\r\n                        <p>Whether you\'re a startup or a global enterprise, learn how to integrate with Front.</p>\r\n                    </div>\r\n                </div>\r\n            </li>\r\n\r\n            <li class=\"step-item\">\r\n                <div class=\"step-content-wrapper\">\r\n                    <span class=\"step-icon step-icon-soft-primary\">3</span>\r\n                    <div class=\"step-content\">\r\n                        <h3>Accelerate your business</h3>\r\n                        <p>We help power millions of businesses to built and run smoothly.</p>\r\n                    </div>\r\n                </div>\r\n            </li>\r\n        </ul>\r\n        \r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(4, "<section>\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            \r\n            <article class=\"col-lg-6 mb-3 mb-sm-5 mb-lg-0\">\r\n                <a class=\"card align-items-end flex-wrap flex-row bg-img-hero text-white h-100 min-h-380rem transition-3d-hover aos-init aos-animate\" href=\"#\" style=\"background-image: url(https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/components/graphics-6.svg);\" data-aos=\"fade-up\">\r\n                    <div class=\"card-body\">\r\n                        <h2 class=\"text-white\">Revolutionizing the way start-ups win new customers</h2>\r\n                        <p>Automate best strategies and focus more on generating hq creatives.</p>\r\n                        <span class=\"font-size-1 font-weight-bold\">Learn More <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            \r\n            \r\n            <article class=\"col-sm-6 col-lg-3 mb-3 mb-sm-0\">\r\n                <a class=\"card align-items-end flex-wrap flex-row bg-img-hero text-white h-100 min-h-380rem transition-3d-hover aos-init aos-animate\" href=\"#\" style=\"background-image: url(https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/components/graphics-7.svg);\" data-aos=\"fade-up\" data-aos-delay=\"100\">\r\n                    <div class=\"card-body\">\r\n                        <h3 class=\"text-white\">How we helped building the industry of the future</h3>\r\n                        <span class=\"font-size-1 font-weight-bold\">Learn More <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            \r\n            \r\n            <article class=\"col-sm-6 col-lg-3\">\r\n                <a class=\"card align-items-end flex-wrap flex-row bg-img-hero text-white h-100 min-h-380rem transition-3d-hover aos-init aos-animate\" href=\"#\" style=\"background-image: url(https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/components/graphics-4.svg);\" data-aos=\"fade-up\" data-aos-delay=\"150\">\r\n                    <div class=\"card-body\">\r\n                        <h3 class=\"text-white\">How to save hundreds of thousands</h3>\r\n                        <span class=\"font-size-1 font-weight-bold\">Learn More <i class=\"fas fa-angle-right fa-sm ml-1\"></i></span>\r\n                    </div>\r\n                </a>\r\n            </article>\r\n            \r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(5, "<section>\r\n    <div class=\"container space-2 space-lg-3\">\r\n        <div class=\"row align-items-lg-center\">\r\n            <div class=\"col-lg-5 order-lg-2 mb-7 mb-lg-0\">\r\n                <div class=\"mb-5\">\r\n                    <h2 class=\"mb-3\">Lightning-fast development with pre-built solutions</h2>\r\n                    <p>Every team has a unique process for shipping software. Use an out-of-the-box workflow, or create one to match the way your team works.</p>\r\n                </div>\r\n\r\n                \r\n                <div class=\"media pb-3\">\r\n                    <span class=\"icon icon-xs icon-soft-indigo icon-circle mr-3\">\r\n                        <i class=\"fas fa-check\"></i>\r\n                    </span>\r\n                    <div class=\"media-body\">\r\n                        <p class=\"text-dark mb-0\">Extensive API documentation</p>\r\n                    </div>\r\n                </div>\r\n                <div class=\"media py-3\">\r\n                    <span class=\"icon icon-xs icon-soft-indigo icon-circle mr-3\">\r\n                        <i class=\"fas fa-check\"></i>\r\n                    </span>\r\n                    <div class=\"media-body\">\r\n                        <p class=\"text-dark mb-0\">Customizable &amp; open source</p>\r\n                    </div>\r\n                </div>\r\n                <div class=\"media pt-3\">\r\n                    <span class=\"icon icon-xs icon-soft-indigo icon-circle mr-3\">\r\n                        <i class=\"fas fa-check\"></i>\r\n                    </span>\r\n                    <div class=\"media-body\">\r\n                        <p class=\"text-dark mb-0\">Continiously improved</p>\r\n                    </div>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-lg-7 order-lg-1\">\r\n                \r\n                <div class=\"pr-lg-4 aos-init aos-animate\" data-aos=\"fade-up\">\r\n                    <div class=\"card shadow-none bg-soft-primary text-indigo text-center w-lg-75 overflow-hidden pt-9 px-5 mx-lg-auto\">\r\n                        <div class=\"px-5 mb-5\">\r\n                            <h3>Choose a workflow, or make your own with Front Software</h3>\r\n                        </div>\r\n\r\n                        \r\n                        <div class=\"device device-half-iphone-x w-85 mx-auto\">\r\n                            <img class=\"device-half-iphone-x-frame\" src=\"https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/components/half-iphone-x.svg\" alt=\"Image Description\">\r\n                            <img class=\"device-half-iphone-x-screen\" src=\"https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/img/407x472/img7.jpg\" alt=\"Image Description\">\r\n                        </div>\r\n                        \r\n                    </div>\r\n                </div>\r\n                \r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(6, "<section>\r\n    <div class=\"position-relative bg-primary overflow-hidden\">\r\n        <div class=\"container space-top-2 space-top-lg-3 position-relative z-index-2\">\r\n            \r\n            <div class=\"w-md-80 w-lg-50 text-center mx-md-auto mb-5 mb-md-9\">\r\n                <h2 class=\"text-white\">Go Front with ease.</h2>\r\n                <p class=\"text-white-70\">Whether you’re a seasoned agile expert, or just getting started, Jira Software unlocks the power of agile.</p>\r\n            </div>\r\n            \r\n            \r\n            <div class=\"w-lg-80 mx-lg-auto mb-11\">\r\n                <div class=\"row mb-9\">\r\n                    <div class=\"col-6 col-md-4 mb-7\">\r\n                        \r\n                        <span class=\"d-block text-white font-size-2 mb-3\">\r\n                            <i class=\"fas fa-align-center\"></i>\r\n                        </span>\r\n                        <h4 class=\"text-white mb-3\">Flexible planning</h4>\r\n                        <p class=\"text-white-70\">Front Software\'s rich planning features enable your team to flexibly plan.</p>\r\n                        \r\n                    </div>\r\n\r\n                    <div class=\"col-6 col-md-4 mb-7\">\r\n                        \r\n                        <span class=\"d-block text-white font-size-2 mb-3\">\r\n                            <i class=\"fas fa-code-branch\"></i>\r\n                        </span>\r\n                        <h4 class=\"text-white mb-3\">Accurate estimations</h4>\r\n                        <p class=\"text-white-70\">Use story points, hours, t-shirt sizes, or your own estimation technique.</p>\r\n                        \r\n                    </div>\r\n\r\n                    <div class=\"col-6 col-md-4 mb-7\">\r\n                        \r\n                        <span class=\"d-block text-white font-size-2 mb-3\">\r\n                            <i class=\"fas fa-shapes\"></i>\r\n                        </span>\r\n                        <h4 class=\"text-white mb-3\">Value-driven prioritization</h4>\r\n                        <p class=\"text-white-70\">Order user stories, issues, and bugs in your product backlog with simple dragging.</p>\r\n                        \r\n                    </div>\r\n\r\n                    <div class=\"col-6 col-md-4 mb-7 mb-md-0\">\r\n                        \r\n                        <span class=\"d-block text-white font-size-2 mb-3\">\r\n                            <i class=\"fas fa-clone\"></i>\r\n                        </span>\r\n                        <h4 class=\"text-white mb-3\">Transparent execution</h4>\r\n                        <p class=\"text-white-70\">Front Software brings a new level of transparency to your team\'s work.</p>\r\n                        \r\n                    </div>\r\n\r\n                    <div class=\"col-6 col-md-4\">\r\n                        \r\n                        <span class=\"d-block text-white font-size-2 mb-3\">\r\n                            <i class=\"fas fa-check-circle\"></i>\r\n                        </span>\r\n                        <h4 class=\"text-white mb-3\">Actionable results</h4>\r\n                        <p class=\"text-white-70\">Extensive reporting functionality gives your team critical insight into their agile process.</p>\r\n                        \r\n                    </div>\r\n\r\n                    <div class=\"col-6 col-md-4\">\r\n                        \r\n                        <span class=\"d-block text-white font-size-2 mb-3\">\r\n                            <i class=\"fas fa-compress\"></i>\r\n                        </span>\r\n                        <h4 class=\"text-white mb-3\">Scalable evolution <span class=\"badge badge-warning badge-pill ml-1\">Coming soon</span></h4>\r\n                        <p class=\"text-white-70\">Front Software is agile project management designed for any teams.</p>\r\n                        \r\n                    </div>\r\n                </div>\r\n\r\n                \r\n                <div class=\"text-center\">\r\n                    <div class=\"mb-3\">\r\n                        <a class=\"btn btn-sm btn-light btn-wide transition-3d-hover\" href=\"#\">Sign up and Start Building</a>\r\n                        <small class=\"d-block d-sm-inline-block text-white-70 my-3 my-sm-0 mx-sm-3\">or</small>\r\n                        <a class=\"btn btn-sm btn-indigo btn-wide transition-3d-hover\" href=\"#\">Talk to our Experts</a>\r\n                    </div>\r\n                    <p class=\"small text-white-70\">Start free trial. * No credit card required.</p>\r\n                </div>\r\n                \r\n            </div>\r\n            \r\n        </div>\r\n\r\n        \r\n        <figure class=\"w-35 position-absolute top-0 right-0\">\r\n            <div class=\"mt-n11 mr-n11\">\r\n                <img class=\"img-fluid\" src=\"https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/components/half-circle-3.svg\" alt=\"Image Description\">\r\n            </div>\r\n        </figure>\r\n        <figure class=\"w-25 position-absolute bottom-0 left-0\">\r\n            <div class=\"mb-n11 ml-n11\">\r\n                <img class=\"img-fluid\" src=\"https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/components/half-circle-2.svg\" alt=\"Image Description\">\r\n            </div>\r\n        </figure>\r\n        \r\n    </div>\r\n</section>\r\n");
            __builder.OpenComponent<Vone.Web.Shared.MainFooter>(7);
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
