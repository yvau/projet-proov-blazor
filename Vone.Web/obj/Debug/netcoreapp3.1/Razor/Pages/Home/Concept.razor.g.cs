#pragma checksum "C:\Users\ymobiot\source\repos\Vone\Vone.Web\Pages\Home\Concept.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7b67086f7ebdbc195368bbb9bf7df2baa486e283"
// <auto-generated/>
#pragma warning disable 1591
namespace Vone.Web.Pages.Home
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Newtonsoft.Json.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Validation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\ymobiot\source\repos\Vone\Vone.Web\_Imports.razor"
using Vone.Shared.ModelViewResults;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/concept")]
    public partial class Concept : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<section class=""mg-md-t-70"">
    <div class=""container"">
        <div class=""row align-items-center"">
            <div class=""col-12 col-md-6 text-center text-sm-left"">
                <div class=""col-fix ml-auto text-center text-sm-left"">
                    <h1 class=""mb-4"">No need of <strong>worrying</strong> about when your home will be <strong>in demand</strong></h1>
                    <p class=""lead text-muted font-size-lg mb-4"">
                        We put proposal from client on the map so that just share your availability and we will price based on popularity of your listing, seasonal factors, nearby events, and more.
                    </p>
                </div>
            </div>
            <div class=""col-12 col-md-6"">

                
                <img src=""/images/illustration-3.png"" alt=""..."" class=""img-fluid mw-md-130"">

            </div>
        </div> 
    </div> 
</section>
");
            __builder.AddMarkupContent(1, "<section class=\"pt-5\" id=\"about\">\r\n    <div class=\"container\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-12 col-md-10 col-lg-8 text-center\">\r\n\r\n                \r\n                <h2 class=\"font-weight-bold\">\r\n                    The hub for all your <span class=\"text-primary\">communication and work</span>.\r\n                </h2>\r\n\r\n                \r\n                <p class=\"font-size-lg text-muted mb-9\">\r\n                    Landkit is where your team can come together to get stuff done. Chat, files, wikis, documentation, and more.\r\n                </p>\r\n\r\n            </div>\r\n        </div> \r\n        <div class=\"row\">\r\n            <div class=\"col-12 col-lg-6\">\r\n\r\n                \r\n                <div class=\"row align-items-center mb-8 aos-init aos-animate\" data-aos=\"fade-up\">\r\n                    <div class=\"col-4 col-lg-5\">\r\n\r\n                        \r\n                        <img src=\"/images/illustration-4.png\" alt=\"...\" class=\"img-fluid\">\r\n\r\n                    </div>\r\n                    <div class=\"col-8 col-lg-7\">\r\n\r\n                        \r\n                        <h3 class=\"font-weight-bold mb-2\">\r\n                            Conversations\r\n                        </h3>\r\n\r\n                        \r\n                        <p class=\"text-gray-700 mb-0\">\r\n                            Communicate with your team in public or private chats with individuals or groups.\r\n                        </p>\r\n\r\n                    </div>\r\n                </div> \r\n                \r\n                <div class=\"row align-items-center mb-8 aos-init aos-animate\" data-aos=\"fade-up\">\r\n                    <div class=\"col-4 col-lg-5\">\r\n\r\n                        \r\n                        <img src=\"/images/illustration-8.png\" alt=\"...\" class=\"img-fluid\">\r\n\r\n                    </div>\r\n                    <div class=\"col-8 col-lg-7\">\r\n\r\n                        \r\n                        <h3 class=\"font-weight-bold mb-2\">\r\n                            Unified\r\n                        </h3>\r\n\r\n                        \r\n                        <p class=\"text-gray-700 mb-0\">\r\n                            Keep everything in one place and feel at peace and organized rather than scattered.\r\n                        </p>\r\n\r\n                    </div>\r\n                </div> \r\n\r\n            </div>\r\n            <div class=\"col-12 col-lg-6\">\r\n                \r\n                <div class=\"row align-items-center mb-8 aos-init aos-animate\" data-aos=\"fade-up\">\r\n                    <div class=\"col-4 col-lg-5\">\r\n\r\n                        \r\n                        <img src=\"/images/illustration-7.png\" alt=\"...\" class=\"img-fluid\">\r\n\r\n                    </div>\r\n                    <div class=\"col-8 col-lg-7\">\r\n\r\n                        \r\n                        <h3 class=\"font-weight-bold mb-2\">\r\n                            Analytics\r\n                        </h3>\r\n\r\n                        \r\n                        <p class=\"text-gray-700 mb-0\">\r\n                            Keep track of what\'s happening in your company from a centralized dashboard.\r\n                        </p>\r\n\r\n                    </div>\r\n                </div> \r\n                \r\n                <div class=\"row align-items-center mb-8 aos-init aos-animate\" data-aos=\"fade-up\">\r\n                    <div class=\"col-4 col-lg-5\">\r\n\r\n                        \r\n                        <img src=\"/images/illustration-6.png\" alt=\"...\" class=\"img-fluid\">\r\n\r\n                    </div>\r\n                    <div class=\"col-8 col-lg-7\">\r\n\r\n                        \r\n                        <h3 class=\"font-weight-bold mb-2\">\r\n                            Permissions\r\n                        </h3>\r\n\r\n                        \r\n                        <p class=\"text-gray-700 mb-0\">\r\n                            Control who has access to which projects and data through our control panel.\r\n                        </p>\r\n\r\n                    </div>\r\n                </div> \r\n\r\n            </div>\r\n        </div> \r\n    </div> \r\n</section>\r\n");
            __builder.AddMarkupContent(2, "<section>\r\n    <div id=\"aboutSection\" class=\"container space-2 space-lg-3 active\">\r\n        \r\n        <div class=\"w-lg-65 text-center mx-auto mb-5 mb-sm-9\">\r\n            <h2 class=\"h1\">What we do?</h2>\r\n            <p>A flexible theme for modern SaaS businesses</p>\r\n        </div>\r\n        \r\n\r\n        <div class=\"row\">\r\n            <div class=\"col-md-4 mb-7\">\r\n                \r\n                <div class=\"text-center px-lg-3\">\r\n                    <figure class=\"max-w-8rem mx-auto mb-4\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-45.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h3>Industry-leading designs</h3>\r\n                    <p>Achieve virtually any design and layout from within the one template.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-md-4 mb-7\">\r\n                \r\n                <div class=\"text-center px-lg-3\">\r\n                    <figure class=\"max-w-8rem mx-auto mb-4\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-14.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h3>Learn from the docs</h3>\r\n                    <p>Whether you\'re a startup or a global enterprise, learn how to integrate with Front.</p>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-md-4 mb-7\">\r\n                \r\n                <div class=\"text-center px-lg-3\">\r\n                    <figure class=\"max-w-8rem mx-auto mb-4\">\r\n                        <img class=\"img-fluid\" src=\"/images/svg/icon-23.svg\" alt=\"SVG\">\r\n                    </figure>\r\n                    <h3>Accelerate your business</h3>\r\n                    <p>We help power millions of businesses to built and run smoothly.</p>\r\n                </div>\r\n                \r\n            </div>\r\n        </div>\r\n\r\n        <img class=\"img-fluid d-none d-md-block w-75 mx-auto mb-7\" src=\"/images/svg/three-pointers.svg\" alt=\"SVG Arrow\">\r\n\r\n        \r\n        <div class=\"w-md-60 w-lg-50 text-center mx-auto mb-7\">\r\n            <p class=\"text-dark\"><span class=\"font-weight-bold\">It is fast and easy.</span> Create your first and ongoing website with Front.</p>\r\n        </div>\r\n        \r\n        \r\n        <div class=\"position-relative w-lg-75 mx-lg-auto mb-4 aos-init aos-animate\" data-aos=\"fade-up\">\r\n            <figure>\r\n                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 1109.8 797.1\">\r\n                    <path fill=\"#f9fbff\" d=\"M105.1,267.1C35.5,331.5-3.5,423,0.3,517.7C6.1,663,111,831.9,588.3,790.8c753-64.7,481.3-358.3,440.4-398.3c-4-3.9-7.9-7.9-11.7-12L761.9,104.8C639.4-27.6,432.5-35.6,299.9,87L105.1,267.1z\"></path>\r\n                </svg>\r\n            </figure>\r\n\r\n            <div class=\"device device-browser\">\r\n                <img class=\"img-fluid\" src=\"/images/svg/browser.svg\" alt=\"Image Description\">\r\n                <img class=\"device-browser-screen\" src=\"/images/img/img.jpg\" alt=\"Image Description\">\r\n            </div>\r\n        </div>\r\n        \r\n\r\n        <div class=\"w-50 w-md-40 w-lg-30 text-center mx-auto\">\r\n            <p class=\"small\">We are launching soon. Join the priority list for information and early access.</p>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(3, "<section>\r\n    <div class=\"container space-top-md-2 space-top-lg-3 text-center\">\r\n        <div class=\"mb-9\">\r\n            <h2 class=\"h1\">What can you do with Front?</h2>\r\n        </div>\r\n\r\n        <div class=\"position-relative pt-5 pb-3 pt-md-11 pb-md-7\">\r\n            <div class=\"position-relative z-index-2 p-2 p-sm-0\">\r\n                \r\n                <div class=\"js-slick-carousel slick slick-equal-height slick-gutters-3 mb-5 slick-initialized slick-slider slick-dotted\" data-hs-slick-carousel-options=\"{\r\n                 &quot;slidesToShow&quot;: 3,\r\n                 &quot;dots&quot;: true,\r\n                 &quot;dotsClass&quot;: &quot;slick-pagination slick-pagination-white d-lg-none mt-5&quot;,\r\n                 &quot;responsive&quot;: [{\r\n                   &quot;breakpoint&quot;: 1200,\r\n                     &quot;settings&quot;: {\r\n                       &quot;slidesToShow&quot;: 3\r\n                     }\r\n                   }, {\r\n                   &quot;breakpoint&quot;: 992,\r\n                   &quot;settings&quot;: {\r\n                     &quot;slidesToShow&quot;: 2\r\n                    }\r\n                   }, {\r\n                   &quot;breakpoint&quot;: 768,\r\n                   &quot;settings&quot;: {\r\n                     &quot;slidesToShow&quot;: 2\r\n                    }\r\n                   }, {\r\n                   &quot;breakpoint&quot;: 554,\r\n                   &quot;settings&quot;: {\r\n                     &quot;slidesToShow&quot;: 1\r\n                   }\r\n                 }]\r\n               }\">\r\n                    \r\n                    <div class=\"slick-list draggable\">\r\n                        <div class=\"slick-track\" style=\"opacity: 1; width: 1140px; transform: translate3d(0px, 0px, 0px);\">\r\n                            <div class=\"js-slide mt-1 mb-3 slick-slide slick-current slick-active\" data-slick-index=\"0\" aria-hidden=\"false\" style=\"width: 350px; height: auto;\" tabindex=\"0\" role=\"tabpanel\" id=\"slick-slide00\" aria-describedby=\"slick-slide-control00\">\r\n                                <div class=\"card flex-wrap flex-row\">\r\n                                    <div class=\"card-body py-6\">\r\n                                        <h3 class=\"mb-5\">Channels</h3>\r\n                                        <figure class=\"max-w-27rem w-100 mx-auto mb-4\">\r\n                                            <img class=\"img-fluid\" src=\"/images/svg/server-woman.svg\" alt=\"Image Description\">\r\n                                        </figure>\r\n                                        <p>Communication in Front happens in channels, organized by project, topic, team, or whatever makes sense for you.</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div><div class=\"js-slide mt-1 mb-3 slick-slide slick-active\" data-slick-index=\"1\" aria-hidden=\"false\" style=\"width: 350px; height: auto;\" tabindex=\"0\" role=\"tabpanel\" id=\"slick-slide01\" aria-describedby=\"slick-slide-control01\">\r\n                                <div class=\"card flex-wrap flex-row\">\r\n                                    <div class=\"card-body py-6\">\r\n                                        <h3 class=\"mb-5\">Integrations</h3>\r\n                                        <figure class=\"max-w-27rem w-100 mx-auto mb-4\">\r\n                                            <img class=\"img-fluid\" src=\"/images/svg/full-stack.svg\" alt=\"Image Description\">\r\n                                        </figure>\r\n                                        <p>Front works with the tools and services you already use every day. Pipe in information or take action without leaving Front.</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div><div class=\"js-slide mt-1 mb-3 slick-slide slick-active\" data-slick-index=\"2\" aria-hidden=\"false\" style=\"width: 350px; height: auto;\" tabindex=\"0\" role=\"tabpanel\" id=\"slick-slide02\" aria-describedby=\"slick-slide-control02\">\r\n                                <div class=\"card flex-wrap flex-row\">\r\n                                    <div class=\"card-body py-6\">\r\n                                        <h3 class=\"mb-5\">Security</h3>\r\n                                        <figure class=\"max-w-27rem w-100 mx-auto mb-4\">\r\n                                            <img class=\"img-fluid\" src=\"/images/svg/business-woman.svg\" alt=\"Image Description\">\r\n                                        </figure>\r\n                                        <p>We take security seriously at Slack. We offer measures like 2FA and SSO to ensure the safety of your data and protect your organization.</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    \r\n                    \r\n                    <ul class=\"slick-pagination slick-pagination-white d-lg-none mt-5\" role=\"tablist\"><li class=\"slick-active\" role=\"presentation\"><span></span></li></ul>\r\n                </div>\r\n\r\n                \r\n                <p class=\"text-white\">\r\n                    See Help Front in action.\r\n                    <a class=\"text-warning font-weight-bold\" href=\"#\">\r\n                        Get a Demo\r\n                        <span class=\"btn btn-xs btn-icon btn-light rounded-circle ml-2\">\r\n                            <i class=\"fas fa-arrow-right\"></i>\r\n                        </span>\r\n                    </a>\r\n                </p>\r\n                \r\n            </div>\r\n\r\n            \r\n            <div class=\"bg-navy position-absolute top-0 right-0 bottom-0 left-0 rounded-lg mx-sm-7\" style=\"background-image: url(/images/svg/abstract-shapes-20.svg);\"></div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.AddMarkupContent(4, "<section>\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-6 mb-3 mb-lg-0\">\r\n                \r\n                <div class=\"card bg-primary text-white h-100 overflow-hidden p-5\">\r\n                    <div class=\"w-65 pr-2\">\r\n                        <h2 class=\"text-white\">Front process</h2>\r\n                        <p class=\"text-white\">Once you are invited to Front Agency, you company will be set within days.</p>\r\n                        <a class=\"btn btn-sm btn-light transition-3d-hover\" href=\"#\">Find Out More <i class=\"fas fa-angle-right ml-1\"></i></a>\r\n                    </div>\r\n                    <div class=\"position-absolute right-0 bottom-0 w-50 mb-n3 mr-n4\">\r\n                        <img class=\"img-fluid\" src=\"/images/img15.png\" alt=\"Image Description\">\r\n                    </div>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            <div class=\"col-lg-6\">\r\n                \r\n                <div class=\"card bg-warning text-white h-100 overflow-hidden p-5\">\r\n                    <div class=\"w-65 pr-2\">\r\n                        <h2 class=\"text-white\">Product design</h2>\r\n                        <p class=\"text-white\">Easy and fast adjustments of elements are possible with Front template.</p>\r\n                        <a class=\"btn btn-sm btn-light transition-3d-hover\" href=\"#\">Find Out More <i class=\"fas fa-angle-right ml-1\"></i></a>\r\n                    </div>\r\n                    <div class=\"position-absolute right-0 bottom-0 w-50 mb-n3 mr-n4\">\r\n                        <img class=\"img-fluid\" src=\"/images/img12.png\" alt=\"Image Description\">\r\n                    </div>\r\n                </div>\r\n                \r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
            __builder.OpenComponent<Vone.Web.Shared.MainFooter>(5);
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
