﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Vone.Shared.Helpers;
using Vone.Shared.Interfaces;
using Vone.Shared.ModelViewResults;

namespace Vone.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IMapper _mapper;
        private IAuthInterface _authInterface;
        private ISmsInterface _smsInterface;
        private IProfileInterface _profileInterface;
        private readonly IConfiguration _configuration;
        private IAutoIncrementInterface _autoIncrementInterface;

        public AuthController(IAuthInterface authInterface,
                              IMapper mapper,
                              IConfiguration configuration,
                              ISmsInterface smsInterface,
                              IProfileInterface profileInterface)
        {
            _authInterface = authInterface;
            _mapper = mapper;
            _configuration = configuration;
            _smsInterface = smsInterface;
            _profileInterface = profileInterface;
        }

        // POST: Auth/Login
        [HttpPost("login/")]
        public async Task<IActionResult> LoginAsync([FromBody] ProfileModelView login)
        {
            var result = _authInterface.SignIn(login.Email, login.Password, false);
            var redirectUrl = String.IsNullOrWhiteSpace(login.RedirectUrl) ? "/" : login.RedirectUrl;

            if (result.status)
            {
                if (result.profile.Enabled ?? false)
                {
                    if (result.profile.TwoWayAuthEnabled ?? false)
                    {
                        if (result.profile.TwoWayAuthType == "qrcodes")
                        {
                            return Ok(new ApiResponse { IsSuccess = true, ContentResult = new { AspTwoFactor = "/auth/verify/qr_code/", RedirectUrl = redirectUrl } });
                        }
                        else if (result.profile.TwoWayAuthType == "sms")
                        {
                            // CacheStore("AspTwoFactorSms", result.profile.Id.ToString());

                            Regex pattern = new Regex("[()\\- ]");
                            String phone = pattern.Replace(result.profile.MobilePhone, "");
                            result.profile.TokenCreatedAt = DateTime.Now;
                            result.profile.TokenId = _smsInterface.SendSms(phone);
                            await _profileInterface.UpdateAsync(result.profile);

                            return Ok(new ApiResponse { IsSuccess = true, ContentResult = new { AspTwoFactor = "/auth/verify/sms_code/", RedirectUrl = redirectUrl } });
                        }
                        else
                        {
                            return Ok(new ApiResponse { IsSuccess = true, ContentResult = new { AspTwoFactor = "null", RedirectUrl = redirectUrl } });
                        }
                    }
                    //
                    string[] roles = result.profile.Role.Split(",");

                    var claims = new[]
                    {
                        new Claim(ClaimTypes.Name, login.Email)
                    };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("Jwt:SecurityKey")));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var expiry = DateTime.Now.AddDays(Convert.ToInt32(_configuration.GetValue<string>("Jwt:ExpiryInDays")));

                    var token = new JwtSecurityToken(
                        _configuration.GetValue<string>("Jwt:Issuer"),
                        _configuration.GetValue<string>("Jwt:Audience"),
                        claims,
                        expires: expiry,
                        signingCredentials: creds
                    );

                    var profile = _mapper.Map<ProfileModelView>(result.profile);
                    profile.RedirectUrl = redirectUrl;
                    profile.TokenId = new JwtSecurityTokenHandler().WriteToken(token);

                    return Ok(new ApiResponse { IsSuccess = true, ContentResult = profile });
                }
                return Ok(new ApiResponse { IsSuccess = false, Message = "Your profile is not enabled yet, If you did not get the email link activation please click on the link below to get a new activation link." });
            }
            return Ok(new ApiResponse { IsSuccess = false, Message = "Your email and password don't match. Please try again." });

            /*if (!result.Succeeded) return BadRequest(new Loginresult { Successful = false, Error = "Username and password are invalid." });

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, login.Email)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecurityKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiry = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JwtExpiryInDays"]));

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: expiry,
                signingCredentials: creds
            );

            return Ok(new Loginresult { Successful = true, Token = new JwtSecurityTokenHandler().WriteToken(token) });*/
        }
    }
}
