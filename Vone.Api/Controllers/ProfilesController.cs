﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Vone.Shared.Context;
using Vone.Shared.Helpers;
using Vone.Shared.Interfaces;
using Vone.Shared.Models;

namespace Vone.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private IAutoIncrementInterface _autoIncrementInterface;
        private IEmailInterface _emailInterface;
        private static Random random = new Random();
        private readonly VoneDbContext _context;

        public ProfilesController(VoneDbContext context,
                                  IAutoIncrementInterface autoIncrementInterface,
                                  IEmailInterface emailInterface)
        {
            _context = context;
            _autoIncrementInterface = autoIncrementInterface;
            _emailInterface = emailInterface;
        }

        // GET: api/Profiles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Profile>>> GetProfile()
        {
            return await _context.Profile.ToListAsync();
        }

        // GET: api/Profiles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Profile>> GetProfile(long id)
        {
            var profile = await _context.Profile.FindAsync(id);

            if (profile == null)
            {
                return NotFound();
            }

            return profile;
        }

        // GET: api/Profiles/token/{token}
        [HttpGet("token/{token}")]
        public async Task<ActionResult<Profile>> GetProfileByToken(string token)
        {
            var profile = await _context.Profile.FirstOrDefaultAsync(x => x.TokenId == token);

            if (profile == null)
            {
                return NotFound();
            }

            return profile;
        }

        // GET: api/Profiles/email/email@example.com
        [HttpGet("email/{email}")]
        public async Task<ActionResult<Profile>> GetProfileByEmail(string email)
        {
            var profile = await _context.Profile.FirstOrDefaultAsync(x => x.Email == email);

            if (profile == null)
            {
                return NotFound();
            }

            return profile;
        }

        // PUT: api/Profiles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProfile(long id, Profile profile, bool? isEmailToSend)
        {
            if (id != profile.Id)
            {
                return BadRequest();
            }

            _context.Entry(profile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            if (isEmailToSend.Value)
            {
                await _emailInterface.SendEmailAsync(profile, "Vone Account activation", Resources.EmailTemplate.RecoverAccount);
            }

            return NoContent();
        }

        // POST: api/Profiles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Profile>> PostProfile(Profile profile)
        {
            profile.Id = _autoIncrementInterface.GetId(Resources.PROFILE); // generate id
            profile.TokenId = RandomString(24); // generate token
            profile.Password = BCrypt.Net.BCrypt.HashPassword(profile.Password); // hash the password
            profile.CreatedAt = DateTime.Now; // assigned Date of creation
            profile.TokenCreatedAt = DateTime.Now;
            profile.Enabled = false; // disable the profile (until it gets verified)
            profile.Role = "ROLE_LIMITED"; // assigned default role

            _context.Profile.Add(profile);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProfileExists(profile.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            // we send the email to the profile
            await _emailInterface.SendEmailAsync(profile, "Vone Account activation", Resources.EmailTemplate.ActivateAccount);

            return CreatedAtAction("GetProfile", new { id = profile.Id }, profile);
        }

        // DELETE: api/Profiles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Profile>> DeleteProfile(long id)
        {
            var profile = await _context.Profile.FindAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            _context.Profile.Remove(profile);
            await _context.SaveChangesAsync();

            return profile;
        }

        private bool ProfileExists(long id)
        {
            return _context.Profile.Any(e => e.Id == id);
        }

        // private function to generate characters (for the token)
        private static String RandomString(int length)
        {
            const String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz@";
            return new String(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}