﻿using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.Helpers;
using Vone.Shared.Interfaces;
using static Vone.Shared.Helpers.Resources;
using Vone.Shared.Models;

namespace Vone.Shared.Services
{
    public class EmailService : IEmailInterface
    {

        private IConfiguration _config;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        public EmailService(IConfiguration config, IRazorViewToStringRenderer razorViewToStringRenderer)
        {
            _config = config;
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        public async Task SendEmailAsync(Profile profile, string subject, EmailTemplate template)
        {
            string bodyHtml = await _razorViewToStringRenderer.RenderViewToStringAsync("/EmailTemplates/" + template + ".cshtml", profile);

            var apiKey = _config["SendGrid:Key"];
            var emailSender = _config["SendGrid:EmailSender"];
            var emailLabel = _config["SendGrid:EmailLabel"];
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(emailSender, emailLabel);
            var to = new EmailAddress(profile.Email);

            var msg = MailHelper.CreateSingleEmail(from, to, subject, "", bodyHtml);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
