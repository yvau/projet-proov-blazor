using System.Linq;
using System.Threading.Tasks;
using System;
using Vone.Shared.Interfaces;
using Vone.Shared.Models;
using Vone.Shared.Context;

namespace Vone.Shared.Services
{
    public class ProfileService : IProfileInterface
    {
        private readonly VoneDbContext _context;

        public ProfileService(VoneDbContext context)
        {
            _context = context;
        }

        // find profile by Email
        public Profile FindByEmail(String email)
        {
            return _context.Profile.FirstOrDefault(x => x.Email == email);
        }

        // find profile by Token
        /*public Profile FindByToken(String token)
        {
            return _context.Profile.FirstOrDefault(x => x.TokenId == token);
        }*/

        // find profile by Token
        public Profile TwoFactor(long id, String verifyCode)
        {
            return _context.Profile.FirstOrDefault(x => x.TokenId == verifyCode && x.Id == id);
        }

        // save profile
        public async Task SaveAsync(Profile profile)
        {
            _context.Add(profile);
            await _context.SaveChangesAsync();
        }


        // update profile
        public async Task UpdateAsync(Profile profile)
        {
            _context.Update(profile);
            await _context.SaveChangesAsync();
        }

    }
}
