﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Vone.Shared.Interfaces;

namespace Vone.Shared.Services
{
    public class SmsService : ISmsInterface
    {
        private IConfiguration _config;
        private static Random random = new Random();

        public SmsService(IConfiguration config)
        {
            _config = config;
        }

        public String SendSms(String phoneNumber)
        {
            // Find your Account Sid and Token at twilio.com/console
            var accountSid = "Twilio:AccountSid";
            var authToken = "Twilio:AuthToken";
            String randomNumber = RandomString(6);

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                body: "authentication code: " + randomNumber,
                from: new Twilio.Types.PhoneNumber("ProovApp"),
                to: new Twilio.Types.PhoneNumber(phoneNumber)
            );

            Console.WriteLine(message);

            return randomNumber;
        }

        // randow string to generate token
        public static String RandomString(int length)
        {
            const String chars = "0123456789";
            return new String(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
