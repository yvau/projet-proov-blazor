﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Components.Authorization;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Components;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Vone.Shared.Helpers;
using Vone.Shared.Interfaces;

namespace Vone.Shared.Services
{
    public class AuthService : IAuthInterface
    {
        private IProfileInterface _profileInterface;
        // private readonly AuthenticationStateProvider _authenticationStateProvider;

        public AuthService(IProfileInterface profileInterface)
        {
            _profileInterface = profileInterface;
            // _authenticationStateProvider = authenticationStateProvider;
        }

        public ProfileModelSignIn SignIn(String Email, String Password, Boolean RememberMe, Boolean FromOauthExternal = false)
        {
            var profile = _profileInterface.FindByEmail(Email);

            if (profile != null)
            {
                if (FromOauthExternal)
                {
                    return new ProfileModelSignIn(true, profile);
                }
                else 
                {
                    Boolean validPassword = BCrypt.Net.BCrypt.Verify(Password, profile?.Password);

                    if (validPassword)
                    {
                        return new ProfileModelSignIn(true, profile);
                    }
                    else
                    {
                        return new ProfileModelSignIn(false, null);
                    }
                }
            }
            return new ProfileModelSignIn(false, null);
        }

        /*public async Task<LoginResult> Login(LoginModelView loginModel)
        {
            var loginAsJson = JsonSerializer.Serialize(loginModel);
            var response = await _httpClient.PostAsync(_config.GetValue<string>("BaseApiUrl") + "api/auth/login", new StringContent(loginAsJson, Encoding.UTF8, "application/json"));

            var loginResult = JsonSerializer.Deserialize<LoginResult>(await response.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            if (!response.IsSuccessStatusCode)
            {
                return loginResult;
            }

            // await _localStorage.SetItemAsync("authToken", loginResult.LoginModelView.Token);
            // ((ApiAuthenticationStateProvider)_authenticationStateProvider).MarkUserAsAuthenticated(loginModel.Email);
            // _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", loginResult.LoginModelView.Token);

            return loginResult;
        }*/

        /*public async Task Logout()
        {
            await _localStorage.RemoveItemAsync("authToken");
            ((ApiAuthenticationStateProvider)_authenticationStateProvider).MarkUserAsLoggedOut();
            _httpClient.DefaultRequestHeaders.Authorization = null;
        }*/

    }
}
