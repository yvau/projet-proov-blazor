﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
// using Vone.Shared.Context;
using Vone.Shared.Interfaces;
// using Vone.Shared.Models;

namespace Vone.Shared.Services
{
    public class AutoIncrementService : IAutoIncrementInterface
    {
        /*private readonly VoneDbContext _context;

        public AutoIncrementService(VoneDbContext context)
        {
            _context = context;
        }*/

        // manual increment id
        public long GetId(String resource)
        {
            // get the autoincrement object based on the parameter {resource}
            // var autoIncrement = _context.AutoIncrement.FirstOrDefault(m => m.Id == resource);

            // assign 0 to the incrementNumber to return
            long incrementNumber = 0;

            // if autoincrement doesn't exist create the data
            /*if (autoIncrement == null)
            {
                AutoIncrement incrementIfNull = new AutoIncrement { Id = resource, IncrementNumber = ++incrementNumber };
                _context.Add(incrementIfNull);
                _context.SaveChanges();
            }

            // if autoincrement doesn't exist update the data add +1 to the IncrementNumber
            else
            {
                incrementNumber = autoIncrement.IncrementNumber.Value;
                autoIncrement.Id = resource;
                autoIncrement.IncrementNumber = ++incrementNumber;

                _context.Update(autoIncrement);
                _context.SaveChanges();
            }*/

            return incrementNumber;
        }

    }
}
