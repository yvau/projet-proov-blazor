using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Vone.Shared.ModelViewResults
{
    public class ProfileModelView
    {
        public long Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public string Language { get; set; }
        public string FacebookHandler { get; set; }
        public string GoogleHandler { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string TokenId { get; set; }
        public string IpAddress { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public bool? Enabled { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime TokenCreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public string RedirectUrl { get; set; }
    }
}
