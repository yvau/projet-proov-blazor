﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.ModelViewResults
{
    public class PropertyModelView
    {
        
        public PropertyModelView()
        {
            // PropertyPhoto = new HashSet<PropertyPhoto>();
            // RequestHasResponse = new HashSet<RequestHasResponse>();
        }

        public long Id { get; set; }
        public string Type { get; set; }
        public string SaleType { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public string Characteristics { get; set; }
        public string Status { get; set; }
        public string Size { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long ProfileId { get; set; }
        public long LocationId { get; set; }

        // public City City { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }

        // public virtual Profile Profile { get; set; }
        // public virtual ICollection<PropertyPhoto> PropertyPhoto { get; set; }
        // public virtual ICollection<RequestHasResponse> RequestHasResponse { get; set; }
    }
}
