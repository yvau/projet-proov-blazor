﻿using System;
using System.Collections.Generic;
using Vone.Shared.Models;

namespace Vone.Shared.ModelViewResults
{
    public partial class BlogModelView
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string TitleSlug { get; set; }
        public string Body { get; set; }
        public bool? IsPublished { get; set; }
        public string ImagePreviewUrl { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Tags { get; set; }
        public long ProfileId { get; set; }
        public bool? Enabled { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
