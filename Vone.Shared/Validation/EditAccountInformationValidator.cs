﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class EditAccountInformationValidator : AbstractValidator<ProfileModelView>
    {
        public EditAccountInformationValidator()
        {

            RuleFor(profileModelView => profileModelView.LastName)
                .NotEmpty();

            RuleFor(profileModelView => profileModelView.Gender)
                .NotEmpty();

            /*RuleFor(profileModelView => profileModelView.BirthDate)
                .NotEmpty();*/
        }
    }
}
