﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.Context;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class RecoverValidator : AbstractValidator<ProfileModelView>
    {
        public RecoverValidator()
        {

            RuleFor(profileModelView => profileModelView.Email)
                .NotEmpty()
                .EmailAddress();
                /*.Must(isEmailExist)
                .WithMessage("EmailDontExist");*/
        }

        private Boolean isEmailExist(String email)
        {
            using (var _db = new VoneDbContext())
            {
                var profile = _db.Profile.FirstOrDefault(x => x.Email == email);
                if (profile != null)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
