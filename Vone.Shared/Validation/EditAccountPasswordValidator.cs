﻿using FluentValidation;
using System;
using System.Linq;
using Vone.Shared.ModelViewResults;
using Vone.Shared.Context;

namespace Vone.Shared.Validation
{
    public class EditAccountPasswordValidator : AbstractValidator<ProfileModelView>
    {
        private string passwordPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[+@#$%]).{6,20})";

        public EditAccountPasswordValidator()
        {

            RuleFor(profileModelView => profileModelView.Password)
            .Must((profileModelView, Password) => BeAValidPassword(Password, profileModelView.Email))
            .WithMessage("PasswordInDatabaseNotValid")
            .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password))
            .NotEmpty();

            RuleFor(profileModelView => profileModelView.NewPassword)
            .NotEmpty()
            .Matches(passwordPattern)
            .WithMessage("PasswordRegexNotValid")
            .NotEqual(profileModelView => profileModelView.Password)
            .WithMessage("PasswordMustBeDifferentFromPrevious");

            /*RuleFor(ProfileModelView => ProfileModelView.RePassword)
            .Equal(profileModelView => profileModelView.NewPassword)
            .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.NewPassword))
            .WithMessage("ConfirmPasswordNotMatching")
            .NotEmpty();*/
        }

        private bool BeAValidPassword(string Password, string Email)
        {
            using (var _db = new VoneDbContext())
            {
                var profile = _db.Profile.FirstOrDefault(x => x.Email == Email);
                if (profile == null)
                {
                    return false;
                }
                else 
                {
                    Boolean validPassword = BCrypt.Net.BCrypt.Verify(Password, profile.Password);
                    if (validPassword)
                    {
                        return true;
                    }

                    return false;
                }
            }
        }
    }
}
