﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class LoginValidator : AbstractValidator<ProfileModelView>
    {

        public LoginValidator()
        {
            RuleFor(profileModelView => profileModelView.Email)
                .NotEmpty();

            RuleFor(profileModelView => profileModelView.Password)
                .NotEmpty();
        }
    }
}
