﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class PropertyValidator : AbstractValidator<PropertyModelView>
    {

        public PropertyValidator()
        {

            /*RuleFor(propertyModelView => propertyModelView.Location.Name)
                .NotEmpty();*/

            RuleFor(propertyModelView => propertyModelView.Type)
               .NotEmpty();

            RuleFor(propertyModelView => propertyModelView.Size)
                .NotEmpty();

            RuleFor(propertyModelView => propertyModelView.Address)
                .NotEmpty();

            RuleFor(propertyModelView => propertyModelView.PostalCode)
                .NotEmpty();
            RuleFor(propertyModelView => propertyModelView.Price)
                .NotEmpty();
            /*RuleFor(proposalModelView => proposalModelView.TypeOfProperties)
                .NotEmpty();

            RuleFor(proposalModelView => proposalModelView.Location)
                .Must(isCollectionNull)
                .WithMessage(locationResource.GetLocalizedHtmlString("EmailDontExist"));

             RuleFor(proposalModelView => proposalModelView.PriceMinimum)
                .LessThanOrEqualTo(proposalModelView => proposalModelView.PriceMaximum)
                .When(proposalModelView => !String.IsNullOrWhiteSpace(proposalModelView.PriceMaximum.ToString())); */
        }

        /*private Boolean isCollectionNull(ICollection<City> locationCollection)
        {
            if (locationCollection.Count > 0)
            {
                return true;
            }
            return false;
        }*/
    }
}
