﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class BlogValidator : AbstractValidator<BlogModelView>
    {
        public BlogValidator()
        {
            RuleFor(blogModelView => blogModelView.Title).NotEmpty();
            RuleFor(blogModelView => blogModelView.Tags).NotEmpty();
            RuleFor(blogModelView => blogModelView.Body).NotEmpty();
        }

    }
}
