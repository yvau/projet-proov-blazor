﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class RoleValidator : AbstractValidator<ProfileModelView>
    {
        public RoleValidator()
        {
            RuleFor(profileModelView => profileModelView.Role).NotEmpty();
        }

    }
}
