﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.Context;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Validation
{
    public class RegisterValidator : AbstractValidator<ProfileModelView>
    {
        private string passwordPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[+@#$%]).{6,20})";

        public RegisterValidator()
        {

            RuleFor(registerModelView => registerModelView.LastName)
                .NotEmpty();

            RuleFor(registerModelView => registerModelView.FirstName)
                .MinimumLength(0)
                .When(registerModelView => !String.IsNullOrWhiteSpace(registerModelView.Password));

            RuleFor(registerModelView => registerModelView.Email)
                .NotEmpty()
                .EmailAddress()
                .Must(isEmailExist)
                .WithMessage("EmailExist");

            RuleFor(registerModelView => registerModelView.Password)
                .NotEmpty()
                .Matches(passwordPattern)
                .WithMessage("PasswordRegexNotValid");

            /*RuleFor(registerModelView => registerModelView.RePassword)
                .Equal(registerModelView => registerModelView.Password)
                .When(registerModelView => !String.IsNullOrWhiteSpace(registerModelView.Password))
                .WithMessage("ConfirmPasswordNotMatching")
                .NotEmpty();*/
        }

        private Boolean isEmailExist(String email)
        {
            using (var _db = new VoneDbContext())
            {
                var profile = _db.Profile.FirstOrDefault(x => x.Email == email);
                if (profile == null)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
