﻿using System;
using System.Collections.Generic;
using Vone.Shared.Models;

namespace Vone.Shared.Interfaces
{
    public interface IJwtInterface
    {
        string GenerateToken(Profile profile);

    }
}
