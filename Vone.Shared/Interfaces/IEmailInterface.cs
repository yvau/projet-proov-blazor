﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.Helpers;
using static Vone.Shared.Helpers.Resources;
using Vone.Shared.Models;

namespace Vone.Shared.Interfaces
{
    public interface IEmailInterface
    {
        Task SendEmailAsync(Profile profile, string subject, EmailTemplate template);
    }
}
