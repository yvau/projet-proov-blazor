﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vone.Shared.Interfaces
{
    public interface IAutoIncrementInterface
    {
        long GetId(String resources);
    }
}
