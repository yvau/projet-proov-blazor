﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vone.Shared.Helpers;

namespace Vone.Shared.Interfaces
{
    public interface IAuthInterface
    {
        ProfileModelSignIn SignIn(String Email, String Password, Boolean RememberMe, Boolean FromOauthExternal = false);
        // ProfileModelConnect SignIn(String Email, String Password, Boolean RememberMe, Boolean FromOauthExternal = false);
        // Task<ApiResponse> Login(ProfileModelView _profileModelView);
    }
}
