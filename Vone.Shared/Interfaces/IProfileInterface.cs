﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vone.Shared.Models;

namespace Vone.Shared.Interfaces
{
    public interface IProfileInterface
    {
        Profile FindByEmail(String email);

        Profile TwoFactor(long id, String verifyCode);

        Task SaveAsync(Profile profile);

        Task UpdateAsync(Profile profile);
    }
}
