﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Vone.Shared.Models;

namespace Vone.Shared.Context
{
    public partial class VoneDbContext : DbContext
    {
        public VoneDbContext()
        {
        }

        public VoneDbContext(DbContextOptions<VoneDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<AutoIncrement> AutoIncrement { get; set; }
        public virtual DbSet<Blog> Blog { get; set; }
        public virtual DbSet<BookmarkProperty> BookmarkProperty { get; set; }
        public virtual DbSet<BookmarkProposal> BookmarkProposal { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationHasProposal> LocationHasProposal { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<PropertyPhoto> PropertyPhoto { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<RequestHasResponse> RequestHasResponse { get; set; }
        public virtual DbSet<RequestHasResponseSatusLog> RequestHasResponseSatusLog { get; set; }
        public virtual DbSet<RequestProposal> RequestProposal { get; set; }
        public virtual DbSet<ResponseProposal> ResponseProposal { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appsettings.{environment}.json", optional: true)
               .Build();

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.ToTable("appointment");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_Appointment_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AllDay).HasColumnName("all_day");

                entity.Property(e => e.AppointmentType).HasColumnName("appointment_type");

                entity.Property(e => e.Caption)
                    .HasColumnName("caption")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Recurrence)
                    .HasColumnName("recurrence")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Appointment)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Appointment_profile1");
            });

            modelBuilder.Entity<AutoIncrement>(entity =>
            {
                entity.ToTable("auto_increment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IncrementNumber).HasColumnName("increment_number");
            });

            modelBuilder.Entity<Blog>(entity =>
            {
                entity.ToTable("blog");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_blog_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Body)
                    .HasColumnName("body")
                    .HasColumnType("text");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.Enabled).HasColumnName("enabled");

                entity.Property(e => e.ImagePreviewUrl)
                    .HasColumnName("image_preview_url")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsPublished).HasColumnName("is_published");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Tags)
                    .HasColumnName("tags")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TitleSlug)
                    .HasColumnName("title_slug")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Blog)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_blog_profile1");
            });

            modelBuilder.Entity<BookmarkProperty>(entity =>
            {
                entity.HasKey(e => new { e.PropertyId, e.ProfileId })
                    .HasName("PK__bookmark__99B01362617A4B1F");

                entity.ToTable("bookmark_property");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_property_has_profile_profile1_idx");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("fk_property_has_profile_property1_idx");

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.BookmarkProperty)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_has_profile_profile1");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.BookmarkProperty)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_has_profile_property1");
            });

            modelBuilder.Entity<BookmarkProposal>(entity =>
            {
                entity.HasKey(e => new { e.ProfileId, e.RequestProposalId })
                    .HasName("PK__bookmark__06B798679D454B9A");

                entity.ToTable("bookmark_proposal");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_profile_has_proposal_profile1_idx");

                entity.HasIndex(e => e.RequestProposalId)
                    .HasName("fk_bookmark_proposal_request_proposal1_idx");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.RequestProposalId).HasColumnName("request_proposal_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.BookmarkProposal)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_profile_has_proposal_profile1");

                entity.HasOne(d => d.RequestProposal)
                    .WithMany(p => p.BookmarkProposal)
                    .HasForeignKey(d => d.RequestProposalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_bookmark_proposal_request_proposal1");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("city");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk_cities_province1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AlternateNames)
                    .HasColumnName("alternate_names")
                    .HasColumnType("text");

                entity.Property(e => e.FCode)
                    .HasColumnName("f_code")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NameAscii)
                    .HasColumnName("name_ascii")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceId)
                    .IsRequired()
                    .HasColumnName("province_id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Timezone)
                    .HasColumnName("timezone")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_cities_province1");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("country");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Continent)
                    .HasColumnName("continent")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyCode)
                    .HasColumnName("currency_code")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyName)
                    .HasColumnName("currency_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Languages)
                    .HasColumnName("languages")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneCode)
                    .HasColumnName("phone_code")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postal_code")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PostalFormat)
                    .HasColumnName("postal_format")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Tld)
                    .HasColumnName("tld")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("location");

                entity.HasIndex(e => e.CityId)
                    .HasName("fk_location_city1_idx");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk_location_country1_idx");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk_location_province1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CityId).HasColumnName("city_id");

                entity.Property(e => e.CountryId)
                    .IsRequired()
                    .HasColumnName("country_id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postal_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceId)
                    .IsRequired()
                    .HasColumnName("province_id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Street)
                    .HasColumnName("street")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_city1");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_country1");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_province1");
            });

            modelBuilder.Entity<LocationHasProposal>(entity =>
            {
                entity.ToTable("location_has_proposal");

                entity.HasIndex(e => e.LocationId)
                    .HasName("fk_location_has_proposal_location1_idx");

                entity.HasIndex(e => e.RequestProposalId)
                    .HasName("fk_location_has_proposal_request_proposal1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.RequestProposalId).HasColumnName("request_proposal_id");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationHasProposal)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_has_proposal_location1");

                entity.HasOne(d => d.RequestProposal)
                    .WithMany(p => p.LocationHasProposal)
                    .HasForeignKey(d => d.RequestProposalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_has_proposal_request_proposal1");
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.ToTable("profile");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bio)
                    .HasColumnName("bio")
                    .HasColumnType("text");

                entity.Property(e => e.BirthDate)
                    .HasColumnName("birth_date")
                    .HasColumnType("date");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomerIdStripe)
                    .HasColumnName("customer_id_stripe")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Enabled).HasColumnName("enabled");

                entity.Property(e => e.FacebookHandler)
                    .HasColumnName("facebook_handler")
                    .HasColumnType("text");

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleHandler)
                    .HasColumnName("google_handler")
                    .HasColumnType("text");

                entity.Property(e => e.IpAddress)
                    .HasColumnName("ip_address")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Language)
                    .HasColumnName("language")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("mobile_phone")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.OfficePhone)
                    .HasColumnName("office_phone")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneConfirmed).HasColumnName("phone_confirmed");

                entity.Property(e => e.PhotoName)
                    .HasColumnName("photo_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PhotoUrl)
                    .HasColumnName("photo_url")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .HasColumnName("role")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TokenCreatedAt)
                    .HasColumnName("token_created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.TokenId)
                    .HasColumnName("token_id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TwoWayAuthEnabled).HasColumnName("two_way_auth_enabled");

                entity.Property(e => e.TwoWayAuthSecret)
                    .HasColumnName("two_way_auth_secret")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TwoWayAuthType)
                    .HasColumnName("two_way_auth_type")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.ToTable("property");

                entity.HasIndex(e => e.LocationId)
                    .HasName("fk_property_location1_idx");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_property_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bathrooms)
                    .HasColumnName("bathrooms")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Bedrooms)
                    .HasColumnName("bedrooms")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Characteristics)
                    .HasColumnName("characteristics")
                    .HasColumnType("text");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.SaleType)
                    .HasColumnName("sale_type")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.YearBuilt).HasColumnName("year_built");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Property)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_location1");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Property)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_profile1");
            });

            modelBuilder.Entity<PropertyPhoto>(entity =>
            {
                entity.ToTable("property_photo");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("fk_property_photo_property1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ContentType)
                    .HasColumnName("content_type")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.PropertyPhoto)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_photo_property1");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.ToTable("province");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk_province_country1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId)
                    .IsRequired()
                    .HasColumnName("country_id")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NameAscii)
                    .HasColumnName("name_ascii")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Province)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_province_country1");
            });

            modelBuilder.Entity<RequestHasResponse>(entity =>
            {
                entity.ToTable("request_has_response");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("fk_proposal_has_proposal_property1_idx");

                entity.HasIndex(e => e.RequestProposalId)
                    .HasName("fk_request_has_response_request_proposal1_idx");

                entity.HasIndex(e => e.ResponseProposalId)
                    .HasName("fk_request_has_response_response_proposal1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AppointmentDate)
                    .HasColumnName("appointment_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.RequestProposalId).HasColumnName("request_proposal_id");

                entity.Property(e => e.ResponseProposalId).HasColumnName("response_proposal_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.RequestHasResponse)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("fk_proposal_has_proposal_property1");

                entity.HasOne(d => d.RequestProposal)
                    .WithMany(p => p.RequestHasResponse)
                    .HasForeignKey(d => d.RequestProposalId)
                    .HasConstraintName("fk_request_has_response_request_proposal1");

                entity.HasOne(d => d.ResponseProposal)
                    .WithMany(p => p.RequestHasResponse)
                    .HasForeignKey(d => d.ResponseProposalId)
                    .HasConstraintName("fk_request_has_response_response_proposal1");
            });

            modelBuilder.Entity<RequestHasResponseSatusLog>(entity =>
            {
                entity.ToTable("request_has_response_satus_log");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_request_has_response_satus_log_profile1_idx");

                entity.HasIndex(e => e.ProfileIdRecipient)
                    .HasName("fk_request_has_response_satus_log_profile2_idx");

                entity.HasIndex(e => e.RequestHasResponseId)
                    .HasName("fk_request_has_response_satus_log_request_has_response1_idx");

                entity.HasIndex(e => e.RequestHasResponseSatusLogId)
                    .HasName("fk_request_has_response_satus_log_request_has_response_satu_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.ProfileIdRecipient).HasColumnName("profile_id_recipient");

                entity.Property(e => e.RequestHasResponseId).HasColumnName("request_has_response_id");

                entity.Property(e => e.RequestHasResponseSatusLogId).HasColumnName("request_has_response_satus_log_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Verb)
                    .HasColumnName("verb")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.RequestHasResponseSatusLogProfile)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_request_has_response_satus_log_profile1");

                entity.HasOne(d => d.ProfileIdRecipientNavigation)
                    .WithMany(p => p.RequestHasResponseSatusLogProfileIdRecipientNavigation)
                    .HasForeignKey(d => d.ProfileIdRecipient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_request_has_response_satus_log_profile2");

                entity.HasOne(d => d.RequestHasResponse)
                    .WithMany(p => p.RequestHasResponseSatusLog)
                    .HasForeignKey(d => d.RequestHasResponseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_request_has_response_satus_log_request_has_response1");

                entity.HasOne(d => d.RequestHasResponseSatusLogNavigation)
                    .WithMany(p => p.InverseRequestHasResponseSatusLogNavigation)
                    .HasForeignKey(d => d.RequestHasResponseSatusLogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_request_has_response_satus_log_request_has_response_satus_1");
            });

            modelBuilder.Entity<RequestProposal>(entity =>
            {
                entity.ToTable("request_proposal");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_request_proposal_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AgeOfProperty)
                    .HasColumnName("age_of_property")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.BankingInstitution)
                    .HasColumnName("banking_institution")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Bathrooms).HasColumnName("bathrooms");

                entity.Property(e => e.Bedrooms).HasColumnName("bedrooms");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.Elevator)
                    .HasColumnName("elevator")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsCurrentlyOwner).HasColumnName("is_currently_owner");

                entity.Property(e => e.IsEnabled)
                    .HasColumnName("is_enabled")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsFirstBuyer)
                    .HasColumnName("is_first_buyer")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsFurnished)
                    .HasColumnName("is_furnished")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsPreApproved)
                    .HasColumnName("is_pre_approved")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsPublished)
                    .HasColumnName("is_published")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsThereContingency).HasColumnName("is_there_contingency");

                entity.Property(e => e.IsWithPool)
                    .HasColumnName("is_with_pool")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbyNavigableWaterBody)
                    .HasColumnName("nearby_navigable_water_body")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbyParc)
                    .HasColumnName("nearby_parc")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbyPublicTransport)
                    .HasColumnName("nearby_public_transport")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbySchool)
                    .HasColumnName("nearby_school")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbySportsCenter)
                    .HasColumnName("nearby_sports_center")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbyTrade)
                    .HasColumnName("nearby_trade")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NearbyWaterfront)
                    .HasColumnName("nearby_waterfront")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfGarage).HasColumnName("number_of_garage");

                entity.Property(e => e.NumberOfParking).HasColumnName("number_of_parking");

                entity.Property(e => e.PriceMaximum)
                    .HasColumnName("price_maximum")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PriceMinimum)
                    .HasColumnName("price_minimum")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TypeOfProperties)
                    .HasColumnName("type_of_properties")
                    .HasColumnType("text");

                entity.Property(e => e.TypeOfProposal)
                    .HasColumnName("type_of_proposal")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.Urgency)
                    .HasColumnName("urgency")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Wheelchair)
                    .HasColumnName("wheelchair")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.RequestProposal)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_request_proposal_profile1");
            });

            modelBuilder.Entity<ResponseProposal>(entity =>
            {
                entity.ToTable("response_proposal");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_request_proposal_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ResponseProposal)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_request_proposal_profile10");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
