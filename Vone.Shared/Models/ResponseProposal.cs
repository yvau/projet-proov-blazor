﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class ResponseProposal
    {
        public ResponseProposal()
        {
            RequestHasResponse = new HashSet<RequestHasResponse>();
        }

        public long Id { get; set; }
        public long ProfileId { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual ICollection<RequestHasResponse> RequestHasResponse { get; set; }
    }
}
