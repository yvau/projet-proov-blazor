﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class AutoIncrement
    {
        public string Id { get; set; }
        public long? IncrementNumber { get; set; }
    }
}
