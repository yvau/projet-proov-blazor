﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class BookmarkProperty
    {
        public long PropertyId { get; set; }
        public long ProfileId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual Property Property { get; set; }
    }
}
