﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class Profile
    {
        public Profile()
        {
            Appointment = new HashSet<Appointment>();
            Blog = new HashSet<Blog>();
            BookmarkProperty = new HashSet<BookmarkProperty>();
            BookmarkProposal = new HashSet<BookmarkProposal>();
            Property = new HashSet<Property>();
            RequestHasResponseSatusLogProfile = new HashSet<RequestHasResponseSatusLog>();
            RequestHasResponseSatusLogProfileIdRecipientNavigation = new HashSet<RequestHasResponseSatusLog>();
            RequestProposal = new HashSet<RequestProposal>();
            ResponseProposal = new HashSet<ResponseProposal>();
        }

        public long Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public bool? Enabled { get; set; }
        public bool? TwoWayAuthEnabled { get; set; }
        public string TwoWayAuthType { get; set; }
        public string TwoWayAuthSecret { get; set; }
        public string TokenId { get; set; }
        public DateTime? TokenCreatedAt { get; set; }
        public string MobilePhone { get; set; }
        public string OfficePhone { get; set; }
        public bool? PhoneConfirmed { get; set; }
        public string IpAddress { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string PhotoUrl { get; set; }
        public string PhotoName { get; set; }
        public string Language { get; set; }
        public string CustomerIdStripe { get; set; }
        public string Bio { get; set; }
        public string FacebookHandler { get; set; }
        public string GoogleHandler { get; set; }

        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Blog> Blog { get; set; }
        public virtual ICollection<BookmarkProperty> BookmarkProperty { get; set; }
        public virtual ICollection<BookmarkProposal> BookmarkProposal { get; set; }
        public virtual ICollection<Property> Property { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> RequestHasResponseSatusLogProfile { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> RequestHasResponseSatusLogProfileIdRecipientNavigation { get; set; }
        public virtual ICollection<RequestProposal> RequestProposal { get; set; }
        public virtual ICollection<ResponseProposal> ResponseProposal { get; set; }
    }
}
