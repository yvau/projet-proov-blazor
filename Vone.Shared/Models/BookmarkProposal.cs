﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class BookmarkProposal
    {
        public long ProfileId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public long RequestProposalId { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual RequestProposal RequestProposal { get; set; }
    }
}
