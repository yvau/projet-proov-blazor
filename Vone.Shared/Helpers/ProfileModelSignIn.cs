﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.Models;

namespace Vone.Shared.Helpers
{
    public class ProfileModelSignIn
    {
        public ProfileModelSignIn(Boolean _status, Profile _profile)
        {
            status = _status;
            profile = _profile;
        }

        public Boolean status { get; set; }
        public Profile profile { get; set; }
    }
}
