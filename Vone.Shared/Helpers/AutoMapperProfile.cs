﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vone.Shared.Models;
using Vone.Shared.ModelViewResults;

namespace Vone.Shared.Helpers
{
    public class AutoMapperProfile : AutoMapper.Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Models.Profile, ProfileModelView>();
            // CreateMap<Models.ProfileSettings, ProfileSettingsModelView>();
            CreateMap<City, CityModelView>();
            // CreateMap<PaginationQuery, PaginationFilter>();
            // CreateMap<Models.Blog, BlogModelView>();
            // CreateMap<Models.Province, ProvinceModelView>();
        }
    }
}
