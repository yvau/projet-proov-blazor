﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vone.Shared.Helpers
{
    public class ApiResponse
    {
        public String Message { get; set; }
        public bool IsSuccess { get; set; }
        public Object ContentResult { get; set; }
    }
}
